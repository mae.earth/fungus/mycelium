/* mae.earth/fungus/mycelium/utilities.go
 * mae 012018 LYHE
 */
package main

import (

	"fmt"
	"time"
	"sort"
	"strconv"
	"strings"
	"sync"
	

	tts "mae.earth/pkg/trustedtimestamps"
	"mae.earth/fungus/mycelium/components"
	"mae.earth/fungus/mycelium/network"
)

const (
	ServiceString string = "_fungus-mycelium-%s_http"
)

var (
	Debug bool = false /* can override with env FUNGUS_MYCELIUM_DEBUG */
)

/* TODO: all the supervisor code for actual binary needs to move to the fungi binary */ 

var internal struct {
	sync.RWMutex
	
	server *tts.Server
	
	environment *components.Environment
	component components.Component

	components map[string]func() components.Component
}

func register(name string,comfunc func() components.Component) {
	internal.Lock()
	defer internal.Unlock()

	internal.components[name] = comfunc
}

func registered() []string {
	internal.RLock()
	defer internal.RUnlock()

	out := make([]string,len(internal.components))
	i := 0

	for k,_ := range internal.components {
		out[i] = k
		i ++
	}

	sort.Slice(out,func(i,j int) bool { return out[i] < out[j] })

	return out
}
	
func snip(str string) string {
	if len(str) < 10 {
		return str
	}
	return str[:7] + "..."
}
	
func truncate(d time.Duration) string {

	str := d.String()

	parts := strings.Split(str,".")
	
	return parts[0] + "s"
}

func lookup(name string) components.Component {
	internal.RLock()
	defer internal.RUnlock()

	comfunc, ok := internal.components[name]
	if ok {
		return comfunc()
	}

	return nil
}

type nullwriter struct{}

func (w nullwriter) Write(p []byte) (n int, err error) {
	return 0, nil
}

/* dedup */
func dedup(s []string, strs ...string) []string {

	p := make(map[string]bool, 0)

	for _, k := range s {
		p[k] = true
	}

	for _, k := range strs {
		p[k] = true
	}

	out := make([]string, len(p))
	i := 0

	for k := range p {
		out[i] = k
		i++
	}

	sort.Slice(out, func(i, j int) bool { return out[i] < out[j] })

	return out
}

/* empty */
func empty(str string) string {
	if len(str) > 0 {
		return str
	}
	return "-"
}

/* flatten */
func flatten(t map[string][]string) string {

	return strings.Join(order(t), ",")
}

func order(t map[string][]string) []string {

	out := make([]string, len(t))
	i := 0
	for k, values := range t {

		if strings.Contains(k,"hash") {
			v := make([]string,len(values))
			if len(values) > 0 {
				copy(v,values)
			}

			for j := 0; j < len(v); j++ {
				v[j] = snip(v[j])
			}
		
			out[i] = fmt.Sprintf("%s=%s",k,strings.Join(v,";"))
		} else {	

			out[i] = fmt.Sprintf("%s=%s", k, strings.Join(values,";"))
		}

		i++
	}

	sort.Slice(out, func(i, j int) bool { return out[i] < out[j] })

	return out
}

func filter(t map[string][]string) map[string][]string {

	out := make(map[string][]string,0)

	for k,values := range t {
		found := false
		for _,tk := range network.HostInformationKeys {
			if k == tk {
				found = true
				break
			}
		}
		if !found {
			out[k] = values
		}
	}

	return out
}


/* parseAddrPort */
func parseAddrPort(addr string) (int, error) {

	parts := strings.Split(addr, ":")
	if len(parts) > 0 {
		d, err := strconv.ParseInt(parts[len(parts)-1], 10, 64)
		if err != nil {
			return -1, err
		}

		return int(d), nil
	}

	return -1, fmt.Errorf("bad address %q", addr)
}
