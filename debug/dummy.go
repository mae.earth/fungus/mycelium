/* mae.earth/fungus/mycelium/debug/debug.go
 * mae 012018 LYHE
 */
package debug

import (
	"sync"
	"time"
	"errors"
	"os"
	"math/rand"

	"image"
	"image/color"
	"image/png"
	_ "image/jpeg"

	"mae.earth/fungus/mycelium/arguments"
)

var (
	ErrAlreadyBegun error = errors.New("Already Begun")
)

type AverageColourSingleFrame struct {
	mux sync.RWMutex
	wg *sync.WaitGroup
	width int
	height int
	ratio int
	input string
	output string

	progressch chan int
}

func (avg *AverageColourSingleFrame) Begin() error {
	avg.mux.Lock()
	defer avg.mux.Unlock()

	if avg.progressch != nil {
		return ErrAlreadyBegun
	}

	f,err := os.Open(avg.input)
	if err != nil {
		return err
	}
	
	m, _, err := image.Decode(f)
	if err != nil {
		return err
	}
	f.Close()

	avg.progressch = make(chan int,1)

	avg.wg = new(sync.WaitGroup)
	avg.wg.Add(1)

	go func() {
		defer avg.wg.Done()
		rect := image.Rect(0,0,avg.width,avg.height)
		img := image.NewRGBA(rect)

		progress := 0

		bw := int(float64(avg.width) / float64(avg.ratio))
		bh := int(float64(avg.height) / float64(avg.ratio))
	
		ox := 0
		oy := 0

		pn := float64(avg.ratio * avg.ratio)
		pt := 1.0 / pn
		pp := 0.0

		avg.progressch <- progress

		for {
			if progress >= 100 {
				break
			}

			r := uint32(0)
			g := uint32(0)
			b := uint32(0)

			//<- time.After(100 * time.Millisecond)
			pixels := 0
			for y := 0; y < bh; y++ {
				for x := 0; x < bw; x++ {

					sr,sg,sb,_ := m.At((ox + x),(oy + y)).RGBA()
					r += sr
					g += sg
					b += sb
					
					pixels++
				}
			}

			r = (r / uint32(pixels))
			g = (g / uint32(pixels))
			b = (b / uint32(pixels))
			

			for y := 0; y < bh; y++ {
				for x := 0; x < bw; x++ {
					
					img.SetRGBA((ox + x),(oy + y),color.RGBA{uint8(r / 256),uint8(g / 256),uint8(b / 256),255})
				}
			}

			ox += bw
			if ox >= avg.width {
				ox = 0
				oy += bh
			}

			pp += pt
			progress = int( (pp / pn) * 100.0 )

			avg.progressch <- progress
		}

		defer close(avg.progressch)
			
		f,err := os.Create(avg.output)
		if err != nil {
			panic(err)
		}
		defer f.Close()

		if err := png.Encode(f,img); err != nil {
			panic(err)
		}
	
	}()

	return nil
}


func (avg *AverageColourSingleFrame) Progress() int {

	return <- avg.progressch
}	

/* Wait */
func (avg *AverageColourSingleFrame) Wait() {
	avg.wg.Wait()
}


/* NewAverageColourSingleFrameTask */
/*
func NewAverageColourSingleFrameTask(width,height,ratio int,input,output string) *AverageColourSingleFrame {

	frame := &AverageColourSingleFrame{width:width,height:height,ratio:ratio,input:input,output:output,progressch:nil}

	return frame
}*/

func NewAverageColourSingleFrameTask(args arguments.Values) *AverageColourSingleFrame {

	frame := &AverageColourSingleFrame{width:500,height:500,ratio:32,input:"",output:""}
	frame.width = args.FirstInt("width",500)
	frame.height = args.FirstInt("height",500)
	frame.ratio = args.FirstInt("ratio",32)
	frame.input = args.FirstString("input","-")
	frame.output = args.FirstString("output","out.png")
	/* TODO */

	return frame
}





type FlatColourSingleFrame struct {
	mux sync.RWMutex
	wg *sync.WaitGroup	
	seed int
	width int
	height int
	ratio int
	output string
	
	average time.Duration

	progressch chan int
}

/* Begin */
func (flat *FlatColourSingleFrame) Begin() error {
	flat.mux.Lock()
	defer flat.mux.Unlock()

	if flat.progressch != nil {
		return ErrAlreadyBegun
	}

	flat.progressch = make(chan int,1)

	flat.wg = new(sync.WaitGroup)
	flat.wg.Add(1)

	go func() {
		defer flat.wg.Done()
		rect := image.Rect(0,0,flat.width,flat.height)
		img := image.NewRGBA(rect)

		progress := 0

		bw := int(float64(flat.width) / float64(flat.ratio))
		bh := int(float64(flat.height) / float64(flat.ratio))
	
		ox := 0
		oy := 0

		pn := float64(flat.ratio * flat.ratio)
		pt := 1.0 / pn
		pp := 0.0

		rand.Seed(int64(flat.seed))

		for {
			if progress >= 100 {
				break
			}

			r := uint8(rand.Intn(255))
			g := uint8(rand.Intn(255))
			b := uint8(rand.Intn(255))

			//<- time.After(100 * time.Millisecond)
			for y := 0; y < bh; y++ {
				for x := 0; x < bw; x++ {
					
					img.SetRGBA((ox + x),(oy + y),color.RGBA{r,g,b,255})
				}
			}

			ox += bw
			if ox >= flat.width {
				ox = 0
				oy += bh
			}

			pp += pt
			progress = int( (pp / pn) * 100.0 )

			flat.progressch <- progress
		}

		defer close(flat.progressch)
			
		f,err := os.Create(flat.output)
		if err != nil {
			panic(err)
		}
		defer f.Close()

		if err := png.Encode(f,img); err != nil {
			panic(err)
		}
	
	}()

	return nil
}			

/* Progress */
func (flat *FlatColourSingleFrame) Progress() int {

	return <- flat.progressch
}	

/* Wait */
func (flat *FlatColourSingleFrame) Wait() {
	flat.wg.Wait()
}


/* NewFlatColourSingleFrameTask */
/*
func NewFlatColourSingleFrameTask(seed,width,height,ratio int,output string) *FlatColourSingleFrame {

	frame := &FlatColourSingleFrame{seed:seed,width:width,height:height,ratio:ratio,output:output,progressch:nil,average:100 * time.Millisecond}

	return frame
}*/

func NewFlatColourSingleFrameTask(args arguments.Values) *FlatColourSingleFrame {

	frame := &FlatColourSingleFrame{seed:101,width:500,height:500,ratio:32,output:"temp.png",progressch:nil,average:100 * time.Millisecond}

	/* FIXME: arguments */
	frame.seed = args.FirstInt("seed",101)
	frame.width = args.FirstInt("width",500)
	frame.height = args.FirstInt("height",500)
	frame.ratio = args.FirstInt("ratio",32)
	frame.output = args.FirstString("output","out.png")


	return frame
}
