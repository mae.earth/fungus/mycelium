/* mae.earth/fungus/mycelium/network/node_test.go
 * mae 012018 LYHE
 */
package network

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_Node(t *testing.T) {
	Convey("Node",t,func() {
		
		n := NewNode("id","foo","bar",101,0)
		So(n,ShouldNotBeNil)
		So(n.Alias,ShouldEqual,"foo")
		So(n.Component,ShouldEqual,"bar")
		So(n.Port,ShouldEqual,101)
		So(n.Weight,ShouldEqual,0)

		So(n.Tags,ShouldBeEmpty)
		So(n.Attributes,ShouldBeEmpty)
		So(n.Information,ShouldBeEmpty)

		So(n.Hash(),ShouldEqual,"3795c41dd3ffa4585e53ff67adc0c45c481f8c883a33352168f08252c8617785f1596d711a9260d7d010048ce674ae02dd38918c107eaebc1f48df26a6720db3")

		So(n.SetTag("foo","bar","soo"),ShouldNotBeNil)
		So(len(n.Tags),ShouldEqual,1)

		So(n.SetAttribute("foo","bar","soo"),ShouldNotBeNil)
		So(len(n.Attributes),ShouldEqual,1)

		So(n.SetInformation("foo","bar","soo"),ShouldNotBeNil)
		So(len(n.Information),ShouldEqual,1)

		So(n.Hash(),ShouldEqual,"bda5ed5cef8d6c4c16c7fc26ce3d65b454a26d7f25a131b43c8e096ba1d46e41894e59580a977d966fe3c7652dede4ef5a0566123abb77d3850dda4943c28d0e")

		

	})
}
