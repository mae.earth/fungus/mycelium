/* mae.earth/fungus/mycelium/network/definitions.go
 * mae 012018 LYHE
 */
package network

import (
	"fmt"
	"sync"
	"time"
	"sort"
	"errors"
	"strings"
	"crypto/sha512"
	"encoding/hex"
)



const (
	NetworkServiceString string = "_fungus-mycelium-%s_http"
	FullNetworkServiceString string = "%s." + NetworkServiceString
)

var (
	ErrBadOptions error = errors.New("Bad Options")
	ErrBadNode error = errors.New("Bad Node")
	ErrNoChange error = errors.New("No Change")
)

/* Options */
type Options struct {
	Local bool /* only select local services */
	Select string /* ; delimited list or empty or * */
}



/* plugin based mycelium network discovery library 
 * the default is to use mDNS
 */

/* Node */
type Node struct {
	mux sync.RWMutex

	Identity string

	Alias string

	
	Port int /* http service port */
	Component string /* name of the component this node refers to */

	Weight int /* nominal and simple for the moment, probably be replaced with a more descriptive system */

	Tags map[string][]string
	Attributes map[string][]string

	Information map[string][]string   /* <- all other information goes here */

	StartTime time.Time

	/* TODO: still to add security timestamps etc */

	/* transient */
	Url string
	Status string /* FIXME: this needs work */

}

func (n *Node) String() string {
	n.mux.RLock()
	defer n.mux.RUnlock()

	return fmt.Sprintf("W%03d\t%s alias=%15s url=%35s with %d tags, %d attributes and %d general",
											n.Weight,n.Identity,n.Alias,n.Url,len(n.Tags),len(n.Attributes),len(n.Information))
}


/* Export */
func (n *Node) Export() (string,[]string) {
	n.mux.RLock()
	defer n.mux.RUnlock()

	out := []string{fmt.Sprintf("alias=%s",n.Alias),fmt.Sprintf("weight=%d",n.Weight),fmt.Sprintf("start-time=%s",n.StartTime.Format(time.UnixDate))}

	for k,values := range n.Tags {
		
		out = append(out,fmt.Sprintf("T:%s=%s",k,strings.Join(values,";")))
	}

	for k,values := range n.Attributes {

		out = append(out,fmt.Sprintf("A:%s=%s",k,strings.Join(values,";")))
	}

	for k,values := range n.Information {
		
		out = append(out,fmt.Sprintf("%s=%s",k,strings.Join(values,";")))
	}


	return fmt.Sprintf(NetworkServiceString,n.Component),out
}

/* SetTagv */
func (n *Node) SetTagv(key string, value interface{}) *Node {
	if value == nil {
		return n
	}

	if str,ok := value.(string); ok {
		return n.SetTag(key, str)
	}

	if i,ok := value.(int); ok {
		return n.SetTag(key,fmt.Sprintf("%d",i))
	}

	if f,ok := value.(float64); ok {
		return n.SetTag(key,fmt.Sprintf("%f",f))
	}

	return n
}

/* SetTag */
func (n *Node) SetTag(key string,values ...string) *Node {
	n.mux.Lock()
	defer n.mux.Unlock()

	if len(values) == 0 {
		return n
	}

	n.Tags[ key ] = values

	return n
}

/* SetAttributev */
func (n *Node) SetAttributev(key string,value interface{}) *Node {
	if value == nil {
		return n
	}

	if str,ok := value.(string); ok {
		return n.SetAttribute(key,str)
	}
	if i,ok := value.(int); ok {
		return n.SetAttribute(key,fmt.Sprintf("%d",i))
	}
	if f,ok := value.(float64); ok {
		return n.SetAttribute(key,fmt.Sprintf("%f",f))
	}
	return n
}

/* SetAttribute */
func (n *Node) SetAttribute(key string,values ...string) *Node {
	n.mux.Lock()
	defer n.mux.Unlock()
	
	if len(values) == 0 {
		return n	
	}

	n.Attributes[ key ] = values

	return n
}

/* SetInformationv */
func (n *Node) SetInformationv(key string,value interface{}) *Node {
	if value == nil {
		return n
	}

	if str,ok := value.(string); ok {
		return n.SetInformation(key,str)
	}
	if i,ok := value.(int); ok {
		return n.SetInformation(key,fmt.Sprintf("%d",i))
	}
	if f,ok := value.(float64); ok {
		return n.SetInformation(key,fmt.Sprintf("%f",f))
	}
	return n
}

/* SetInformation */
func (n *Node) SetInformation(key string,values ...string) *Node {
	n.mux.Lock()
	defer n.mux.Unlock()

	if len(values) == 0 {
		return n
	}

	n.Information[ key ] = values

	return n
}

func (n *Node) Update() *Node {
	n.mux.Lock()
	defer n.mux.Unlock()

	n.Information = make(map[string][]string,0)

	return n
}
	

/* Hash */
func (n *Node) Hash() string {
	n.mux.RLock()
	defer n.mux.RUnlock()

	h := sha512.New()
	h.Write([]byte(n.Identity))
	h.Write([]byte(n.Alias))
	h.Write([]byte(fmt.Sprintf("%d",n.Port)))
	h.Write([]byte(n.Component))
	h.Write([]byte(fmt.Sprintf("%d",n.Weight)))
		
	if len(n.Tags) > 0 {
		out := make([]string,0)
		for k,values := range n.Tags {
			if len(values) > 0 {
				out = append(out, fmt.Sprintf("%s=%s",k,strings.Join(values,";")))
			}
		}
		sort.Slice(out,func(i,j int) bool { return out[i] < out[j] })
	
		for _,k := range out {
			h.Write([]byte(k))
		}
	}

	if len(n.Attributes) > 0 {
		out := make([]string,0)
		for k,values := range n.Attributes {
			if len(values) > 0 {
				out = append(out, fmt.Sprintf("%s=%s",k,strings.Join(values,";")))
			}
		}
		sort.Slice(out,func(i,j int) bool { return out[i] < out[j] })

		for _,k := range out {
			h.Write([]byte(k))
		}
	}

	if len(n.Information) > 0 {
		out := make([]string,0)
		for k,values := range n.Information {
			if len(values) > 0 {
				out = append(out, fmt.Sprintf("%s=%s",k,strings.Join(values,";")))
			}
		}
		sort.Slice(out,func(i,j int) bool { return out[i] < out[j] })

		for _,k := range out {
			h.Write([]byte(k))
		}
	}

	return hex.EncodeToString(h.Sum(nil))
}

/* NewNode */
func NewNode(id,alias, component string, port, weight int) *Node {

	n := &Node{Identity:id,Alias: alias, Port: port,Component: component,Weight:weight,StartTime: time.Now()}
	n.Tags = make(map[string][]string,0)
	n.Attributes = make(map[string][]string,0)
	n.Information = make(map[string][]string,0)

	return n
}


	


