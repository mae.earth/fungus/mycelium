/* mae.earth/fungus/mycelium/network/tools.go
 * mae 012018 LYHE
 */
package network

import (
	"sort"
	"crypto/sha1"
	"encoding/hex"
)

var (
	HostInformationKeys = []string{"platform","ram"}
)




/* GraphNode */
type GraphNode struct {
	Id string
	Host string
	Components map[string] []*Node
	Information map[string] []string
}

type Graph []*GraphNode

func (g Graph) Lookup(id string) *GraphNode {

	for _,node := range g {
		if id == node.Id {
			return node
		}
	}
	
	return nil
}


/* GraphArrange */
func GraphArrange(nodes []*Node) Graph {

	/* FIXME: BUG, if set alias then end up with nil gnodes ? */

	/* first find all the hosts */
	hosts := make(map[string]int,0)

	for _, node := range nodes {
		if node == nil || node.Alias == "" || node.Component == "" {
			continue
		}

		if _,exists := hosts[ node.Alias ]; !exists {
			hosts[ node.Alias ] = 0
		}
		hosts[ node.Alias ] ++
	}
	
	if len(hosts) == 0 {
		return nil
	}

	gnodes := make([]*GraphNode,len(hosts))	
		
	i := 0
	for name,count := range hosts {		
		
		h := sha1.New()
		h.Write([]byte(name))
		
		gnodes[i] = &GraphNode{Id: hex.EncodeToString(h.Sum(nil)), Host:name, 
													 Components: make(map[string][]*Node,0), Information: make(map[string][]string,0)}

		/* go through all the nodes and add to the components */
		n := 0
		for _,node := range nodes {
			if node == nil || node.Alias == "" || node.Component == "" {
				continue
			}

			if node.Alias == name {
				n++				
				if l,exists := gnodes[i].Components[ node.Component ]; exists {

					l = append(l, node)
					gnodes[i].Components[ node.Component ] = l
				} else {

					gnodes[i].Components[ node.Component ] = []*Node{ node }
				}
			}	
				
			if n >= count {
				break
			}
		}


		/* go through and move some of the information to the host; such as os, ram etc */
		for _,component := range gnodes[i].Components {
			for _,node := range component {
				for key,values := range node.Information {

					/* TODO: append not replace then reduce to unique */

					for _,match := range HostInformationKeys {
						if key == match {
							gnodes[i].Information[ key ] = values
						}
					}
				} 
			}
		}

		
		/* sort by weight */
		fin := make(map[string][]*Node,0)

		for component,list := range gnodes[i].Components {
	
			sort.Slice(list, func(i,j int) bool { return list[i].Weight < list[j].Weight })

			fin[component] = list					
		}

		gnodes[i].Components = fin

		i++ /* mae -- added because it seemed to be missing */
	}

	return gnodes
}
