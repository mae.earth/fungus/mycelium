/* mae.earth/fungus/mycelium/network/mdns/plugin.go
 * mae 012018 LYHE
 */
package mdns

import (
	"os"
	"fmt"	
	"net"
	"time"
	"sync"
	"strconv"
	"strings"
	"github.com/hashicorp/mdns"
	. "mae.earth/fungus/mycelium/network"

)


/* this plugin handles two operations
 * 1. exporting nodes to the network (announcement)
 * 2. importing nodes from the network (discovery)
 */

/* FIXME:
 * compact this code into the parent package then add a tree mechanism 
 */

var internal struct {
	mux sync.RWMutex

	current string
	cycle int
	server *mdns.Server
}

/* Announce */
func Announce(node *Node) error {
	internal.mux.Lock()
	defer internal.mux.Unlock()

	if node == nil {
		return ErrBadNode
	}queue/admin

	hash := node.Hash()

	if hash == internal.current {
		return ErrNoChange
	}

	if internal.server != nil {
		internal.server.Shutdown()
	}

	hostname,err := os.Hostname()
	if err != nil {
		return err
	}

	internal.cycle ++

	name,info := node.Export()

	//fmt.Printf("exporting %s\n",name)
	info = append(info,fmt.Sprintf("A:cycle=%d",internal.cycle))

	service,err := mdns.NewMDNSService(node.Identity,name,"local.",
																		 hostname + ".", node.Port, nil, info) /* NOTE: do not use strings.ToLower on the hostname; must be as set */

	if err != nil {
		return err
	}

	/* FIXME: add backoff code here to avoid any network binding errors */

	

	server,err := mdns.NewServer(&mdns.Config{Zone: service})
	if err != nil {
		return err
	}

	/* TODO: backoff code */

	internal.current = hash
	internal.server = server

	return nil
}


/* Discovery */
func Discovery(options *Options) ([]*Node,error) {

	if options == nil {
		return nil,ErrBadOptions
	}

	if options.Select == "" {
		return nil,ErrBadOptions
	}

	/* define a useful lookup function */
	lookup := func(servicename string, entriesch chan *mdns.ServiceEntry) error {

		full := fmt.Sprintf(NetworkServiceString, servicename)
	
		return mdns.Query(&mdns.QueryParam{ Service: full, 
																			  Domain: "local.",
																				Timeout: 100 * time.Millisecond,
																				Entries: entriesch,
																				WantUnicastResponse: false})
	}


	services := make([]*mdns.ServiceEntry,0)
	entriesch := make(chan *mdns.ServiceEntry,100)

	/* setup filter functions here */
	filter := func(n *mdns.ServiceEntry) bool { return true }

	if options.Local {

		addrs,err := net.InterfaceAddrs()
		if err != nil {
			return nil,err
		}

		straddrs := []string{}
		for _,addr := range addrs {
			o := addr.String()
			if strings.Contains(o,"/") {
				o = o[:strings.Index(o,"/")]
			}
			straddrs = append(straddrs,o)
		}

		filter = func(entry *mdns.ServiceEntry) bool {

			add := ""

			if entry.AddrV4 != nil {
				
				add = entry.AddrV4.String()
			} else if entry.AddrV6 != nil {

				add = entry.AddrV6.String()
			}

			if add == "" {
				return false
			}

			for _,addr := range straddrs {
				if addr == add {
					return true
				}
			}

			return false
		}
	}

	go func() {
		for entry := range entriesch {
			
			if filter(entry) {
				services = append(services, entry)
			}
		}
	}()

	for _,key := range strings.Split(options.Select,";") {
		
		/* FIXME: log the any errors; erroring here has the problem of network down will terminate process */
		lookup(key,entriesch)
		/*
		if err := lookup(key,entriesch); err != nil {
			return nil,err
		}*/
	}

	close(entriesch)
	time.Sleep(50 * time.Millisecond)

	/* convert to node */
	nodes := make([]*Node,0)

	for _,service := range services {

		/* pitch the node id from the name */
		alias := "-"
		id := "-"
		component := "-"
		url := "-"
		status := "busy" /* assume busy */
		weight := 0
		var startTime time.Time 

		/* FIXME: the protocol is hard-coded */ 
		if service.AddrV4 != nil {
			url = fmt.Sprintf("http://%s:%d/", service.AddrV4, service.Port)
		} else if service.AddrV6 != nil {
			url = fmt.Sprintf("http://%s:%d/", service.AddrV6, service.Port)
		}

		general := make(map[string]string, 0)
		tags := make(map[string]string, 0)
		atts := make(map[string]string, 0)

		parts := strings.Split(service.Name, ".")
		if len(parts) > 0 {
			id = parts[0]

			if len(parts) > 1 {
				/* TODO: this should not be hard-coded */
				component = strings.TrimPrefix(parts[1], "_fungus-mycelium-")
				component = strings.TrimSuffix(component, "_http")
			}
		}

		for _, str := range strings.Split(service.Info, "|") {

			if !strings.Contains(str, "=") {
				continue
			}

			parts := strings.Split(str, "=")

			if len(parts) != 2 {
				continue
			}

			/* tags */
			if strings.HasPrefix(parts[0], "T:") {

				tags[strings.TrimPrefix(parts[0], "T:")] = parts[1]

			} else if strings.HasPrefix(parts[0], "A:") {

				atts[strings.TrimPrefix(parts[0], "A:")] = parts[1]

			} else {

				switch parts[0] {
				case "weight":
					d,err := strconv.ParseInt(parts[1],10,64)
					if err == nil {
						weight = int(d)
					}
				break
				case "alias":

					alias = parts[1]
					break
				case "start-time":

					t, err := time.Parse(time.UnixDate, parts[1])
					if err == nil {
						startTime = t
					}
					break
				case "status":

					/* TODO: status will be a complex number based format with maybe tags, but for now it's just a int */
					d, err := strconv.ParseInt(parts[1], 10, 64)
					if err == nil {
						if d < 0 {
							status = "error"
						} else if d == 0 {
							status = "available"
						} else {
							status = "busy"
						}
					}

					break
				default:
					general[parts[0]] = parts[1]
					break
				}
			}
		}

		n := NewNode(id,alias, component, service.Port, weight)
		n.StartTime = startTime
		n.Url = url
		n.Status = status

		for key,values := range tags {

			n.Tags[key] = strings.Split(values,";")
		}

		for key,values := range atts {
			
			n.Attributes[key] = strings.Split(values,";")
		}

		for key,values := range general {
			
			n.Information[key] = strings.Split(values,";")
		}

		nodes = append(nodes,n)
	}


	return nodes,nil
}






