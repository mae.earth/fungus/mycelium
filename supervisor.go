/* mae.earth/fungus/mycelium/supervisor.go
 * mae 012018 LYHE
 */
package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"html/template"
	"os"
	"os/signal"
	"strings"
	"time"
	"path"
	"crypto/sha1"
	"encoding/hex"

	"github.com/dustin/go-humanize"
	"github.com/gorilla/mux"
	"github.com/olekukonko/tablewriter"
	viz "github.com/awalterschulze/gographviz"
	"github.com/GeertJohan/go.rice" /* embeddable resources */

	tts "mae.earth/pkg/trustedtimestamps"

	/* components that mycelium understands */
	"mae.earth/fungus/mycelium/components"
	"mae.earth/fungus/mycelium/components/idle"
	"mae.earth/fungus/mycelium/components/worknode"
	"mae.earth/fungus/mycelium/components/queue"
	"mae.earth/fungus/mycelium/components/store"


	/* required components */
	"mae.earth/fungus/mycelium/host"
	"mae.earth/fungus/mycelium/arguments"
	"mae.earth/fungus/mycelium/network"
	. "mae.earth/fungus/mycelium/network/mdns"

	"mae.earth/fungus/mycelium/debug" /* used for debugging */

	"mae.earth/pkg/build"

	//"gitlab.com/mae.earth/fungus/mycelium/ll/agent/std"
)

/* TODO: all the supervisor code for actual binary needs to move to the fungi binary */

func init() {

	internal.components = make(map[string]func() components.Component, 0)

	log.SetOutput(nullwriter{})

	debug := os.Getenv("FUNGUS_MYCELIUM_DEBUG")
	if debug != "" {
		Debug = true
	}

	/* register all components here */

	register(idle.Name, func() components.Component { return idle.New() })
	register(worknode.Name, func() components.Component { return worknode.New() })
	register(queue.Name, func() components.Component { return queue.New() })
	register(store.Name, func() components.Component { return store.New() })

}

/* TODO:
 *
 * Break out the network code to own package
 * Supervisor does all the network scanning and passes components found as an interested list to the active component
 * Do a local scan before beginning component, if the same component then exit
 */

/* FIXME:
 *
 * mDNS requires a IP address to work! build in some backoff code to solve this.
 */

/*
 * unique id is basically the hostname + a generated id (loose)
 */

func main() {

	man := func() {
		if bs, ok := build.String(); ok {
			fmt.Printf("%s\n", bs)
			fmt.Printf("build hash: %s\n", build.Hash())
			fmt.Printf("git hash: %s\n\n", build.GitHash)
		}
		fmt.Printf("subcommands :-\n")

		fmt.Printf("\tversion\n\tnetwork\n\tlocal\n\tdebug\n\tagent\n")

		for _, com := range registered() {
			fmt.Printf("\t%s\n", com)
		}
	}

	if len(os.Args) < 2 {
		man()
		os.Exit(0)
	}

	/* parse the arguments for common flags */

	switch os.Args[1] {
	case "version":
		fmt.Printf("███████╗██╗   ██╗███╗   ██╗ ██████╗ ██╗   ██╗███████╗           ███╗   ███╗██╗   ██╗ ██████╗███████╗██╗     ██╗██╗   ██╗███╗   ███╗\n")
		fmt.Printf("██╔════╝██║   ██║████╗  ██║██╔════╝ ██║   ██║██╔════╝    ██╗    ████╗ ████║╚██╗ ██╔╝██╔════╝██╔════╝██║     ██║██║   ██║████╗ ████║\n")
		fmt.Printf("█████╗  ██║   ██║██╔██╗ ██║██║  ███╗██║   ██║███████╗    ╚═╝    ██╔████╔██║ ╚████╔╝ ██║     █████╗  ██║     ██║██║   ██║██╔████╔██║\n")
		fmt.Printf("██╔══╝  ██║   ██║██║╚██╗██║██║   ██║██║   ██║╚════██║    ██╗    ██║╚██╔╝██║  ╚██╔╝  ██║     ██╔══╝  ██║     ██║██║   ██║██║╚██╔╝██║\n")
		fmt.Printf("██║     ╚██████╔╝██║ ╚████║╚██████╔╝╚██████╔╝███████║    ╚═╝    ██║ ╚═╝ ██║   ██║   ╚██████╗███████╗███████╗██║╚██████╔╝██║ ╚═╝ ██║\n")
		fmt.Printf("╚═╝      ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝  ╚═════╝ ╚══════╝           ╚═╝     ╚═╝   ╚═╝    ╚═════╝╚══════╝╚══════╝╚═╝ ╚═════╝ ╚═╝     ╚═╝\n")
		fmt.Printf("\n\n")

		if bs, ok := build.String(); ok {
			fmt.Printf("%s\n", bs)
			fmt.Printf("build hash: %s\n", build.Hash())
			fmt.Printf("git hash: %s\n", build.GitHash)
		} else {
			fmt.Printf("version unknown\n")
		}
		os.Exit(0)

		break
	case "network":

		nodes, err := Discovery(&network.Options{Local: false, Select: strings.Join(registered(), ";")})
		if err != nil {
			fmt.Printf("discovery error -- %v\n", err)
			os.Exit(1)
		}

		simple := false
		dot := false

		if len(os.Args) >= 3 {
			if os.Args[2] == "simple" {
				simple = true
			}
			if os.Args[2] == "dot" {
				dot = true
			}
		}

		/* Simple render */
		if simple {
			for _, node := range nodes {

				fmt.Printf("%s - %s, %q\n\ttags : %s\n\tattributes : %s\n\tinformation : %s\n\n\tuptime %s\n\turl %s\n\tstatus is %s\n\n",
					node.Alias, node.Component, node.Identity, empty(flatten(node.Tags)), empty(flatten(node.Attributes)), empty(flatten(node.Information)),
					truncate(time.Since(node.StartTime)), node.Url, node.Status)
			}
			os.Exit(0)
		}

		/* Dot render */
		if dot {

			E := func(str string) string {
				return fmt.Sprintf("%q",str)
			}

			hostname, _ := os.Hostname()
			
			/* scan for hostname in the set; if present then we don't need a root node */
			h := sha1.New()
			h.Write([]byte(hostname))
			
			id := hex.EncodeToString(h.Sum(nil))


			root := "mycn"

			g := viz.NewGraph()
			g.SetName(root)
			g.SetDir(true)

			graph := network.GraphArrange(nodes)


			/* TODO: look for node in the graph if found then use that */
			attrs := make(map[string]string,0)
			attrs[string(viz.Shape)] = "Mrecord"
		
			if gnode := graph.Lookup(id); gnode != nil {

				label := fmt.Sprintf("{ %s }",hostname)

				if len(gnode.Information) > 0 {
					alabel := ""
					for _,values := range order(gnode.Information) {
						if len(alabel) > 0 {
							alabel += "|" 
						}
						alabel += fmt.Sprintf("{%s}", values)
					}

					label += fmt.Sprintf("|{ Information | { %s }}", alabel)
				}


				attrs[string(viz.Label)] = E(fmt.Sprintf("{ %s }",label))

			} else {
				attrs[string(viz.Label)] = E(fmt.Sprintf("{ %s }",hostname))
			}

			attrs[string(viz.Style)] = "filled"
			attrs[string(viz.FillColor)] = "yellow"
			attrs[string(viz.FontSize)] = "20"

			g.AddNode(root,E(id),attrs)


			/* TODO: run through an annotation suite */

			/* arrange into weight, lower weights are bigger in size. Components on the same node are grouped into a linear by weight. */			
			for _,gnode := range network.GraphArrange(nodes) {

				if id != gnode.Id {

					attrs[string(viz.Shape)] = "Mrecord"
					attrs[string(viz.FillColor)] = "white"
				
					label := fmt.Sprintf("{ %s }",gnode.Host)

					if len(gnode.Information) > 0 {
						alabel := ""
						for _,values := range order(gnode.Information) {
							if len(alabel) > 0 {
								alabel += "|" 
							}
							alabel += fmt.Sprintf("{%s}", values)
						}

						label += fmt.Sprintf("|{ Information | { %s }}", alabel)
					}

					attrs[string(viz.Label)] = E(fmt.Sprintf("{ %s }",label))
					attrs[string(viz.FontSize)] = "16"

					g.AddNode(root, E(gnode.Id), attrs)
					g.AddEdge(E(id),E(gnode.Id), true, nil)
				}

				for component, list := range gnode.Components {

					/* TODO: status colours */
					attrs[string(viz.FillColor)] = "white"
					attrs[string(viz.Shape)] = "circle"					
					attrs[string(viz.Label)] = E(component)
					attrs[string(viz.FontSize)] = "12"

					g.AddNode(root, E(gnode.Id + "/" + component), attrs)
					g.AddEdge(E(gnode.Id), E(gnode.Id + "/" + component), true, nil)

					last := gnode.Id + "/" + component

					for _, n := range list {

						label := fmt.Sprintf("{ Weight %d | %s}", n.Weight, n.Identity[:8])
						
						if len(n.Attributes) > 0 {
							alabel := ""						
							for _,values := range order(n.Attributes) {
								if len(alabel) > 0 {
									alabel += "|"
								}						
								alabel += fmt.Sprintf("{%s}", values)
							}

							label += fmt.Sprintf("|{ Attributes | { %s }}", alabel)
						}

						if len(n.Tags) > 0 {
							alabel := ""
							for _,values := range order(n.Tags) {
								if len(alabel) > 0 {
									alabel += "|"
								}
								alabel += fmt.Sprintf("{%s}",values)
							}
			
							label += fmt.Sprintf("|{ Tags | { %s }}", alabel)
						}

						if len(n.Information) > 0 {
							alabel := ""
							for _,values := range order(filter(n.Information)) {
								if len(alabel) > 0 {
									alabel += "|" 
								}
								alabel += fmt.Sprintf("{%s}", values)
							}

							label += fmt.Sprintf("|{ Information | { %s }}", alabel)
						}

						label += fmt.Sprintf("| { Uptime %s | Url %s}", truncate(time.Since(n.StartTime)), n.Url)
	
						attrs[string(viz.Shape)] = "Mrecord"
						attrs[string(viz.Label)] = E(fmt.Sprintf("{%s}",label))
						attrs[string(viz.FontSize)] = "10"

						g.AddNode(root, E(n.Identity), attrs)
						g.AddEdge(E(last), E(n.Identity), true, nil)

						last = n.Identity			
					}
				}				
			}		
			
			fmt.Printf("%s\n", g.String())
			os.Exit(0)
		}
				

		/* default */
		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"Alias", "Component Type", "Identity", "Tags", "Attributes", "Info", "Uptime", "URL", "Status"})
		table.SetAutoMergeCells(true)
		table.SetRowLine(true)
		table.SetCaption(true, fmt.Sprintf("> %s", time.Now().Format(time.UnixDate)))

		for _, node := range nodes {

			table.Append([]string{node.Alias, node.Component, node.Identity,
				empty(flatten(node.Tags)),
				empty(flatten(node.Attributes)),
				empty(flatten(node.Information)),
				truncate(time.Since(node.StartTime)), node.Url, node.Status})
		}

		table.Render()
		os.Exit(0)

		break
	case "local":

		nodes, err := Discovery(&network.Options{Local: true, Select: strings.Join(registered(), ";")})
		if err != nil {
			fmt.Printf("discovery error -- %v\n", err)
			os.Exit(1)
		}

		simple := false

		if len(os.Args) >= 3 {
			if os.Args[2] == "simple" {
				simple = true
			}
		}

		if !simple {
			table := tablewriter.NewWriter(os.Stdout)
			table.SetHeader([]string{"Alias", "Component Type", "Identity", "Tags", "Attributes", "Info", "Uptime", "URL", "Status"})
			table.SetAutoMergeCells(true)
			table.SetRowLine(true)
			table.SetCaption(true, fmt.Sprintf("> %s", time.Now().Format(time.UnixDate)))

			for _, node := range nodes {

				table.Append([]string{node.Alias, node.Component, node.Identity,
					empty(flatten(node.Tags)),
					empty(flatten(node.Attributes)),
					empty(flatten(node.Information)),
					truncate(time.Since(node.StartTime)), node.Url, node.Status})
			}

			table.Render()
		} else {

			for _, node := range nodes {

				fmt.Printf("%s - %s, %q\n\ttags : %s\n\tattributes : %s\n\tinformation : %s\n\n\tuptime %s\n\turl %s\n\tstatus is %s\n\n",
					node.Alias, node.Component, node.Identity, empty(flatten(node.Tags)), empty(flatten(node.Attributes)), empty(flatten(node.Information)),
					truncate(time.Since(node.StartTime)), node.Url, node.Status)
			}

		}

		os.Exit(0)
		break
	case "agent": 

		/* standard agent mode (as default) runs using stdin, stdout, stderr */

		//agent := std.New()
		
				


		break
	case "debug":

		/* run the dummy {task} process -- normally worknodes will call this for unit testing */
		//fmt.Printf("running dummy task process with\n")
		//fmt.Printf("\t%v\n",os.Args[2:])

		/* TODO: also wondering if this process should be continously looped? */

		args := arguments.New() /* empty */

		if len(os.Args) < 3 {
			fmt.Printf("require task :-\n\tFlatColourSingleFrame\n\tAvos/#GetpiderageColourSingleFrame\n")
			os.Exit(1)
		}

		/* parse the args */
		args,err := arguments.Parse(os.Args,3)
		if err != nil {
			if err != arguments.ErrEmpty {
				fmt.Fprintf(os.Stderr,"error whilst parsing arguments -- %v\n",err)
				os.Exit(1)
			} 
		}

		fmt.Printf("arguments :-\n%s\n",args)

		switch os.Args[2] {
		case "FlatColourSingleFrame":

			flat := debug.NewFlatColourSingleFrameTask(args)

			if err := flat.Begin(); err != nil {
				fmt.Fprintf(os.Stderr, "%v", err)
				os.Exit(1)
			}

			last := 0

			for {
				progress := flat.Progress()
				if progress != last {
					last = progress
					if progress > 100 {
						progress = 100
					}
					fmt.Fprintf(os.Stdout, "\r%03d%%", progress)
				}
				if progress >= 100 {
					break
				}
			}

			fmt.Fprintf(os.Stdout, "\n")

			flat.Wait()
			break
		case "AverageColourSingleFrame":

			avg := debug.NewAverageColourSingleFrameTask(args) /*(2240, 1480, 3, "./dummy/harewood-house.jpg", "temp.png") */

			if err := avg.Begin(); err != nil {
				fmt.Fprintf(os.Stderr, "%v", err)
				os.Exit(1)
			}

			last := 0

			for {
				progress := avg.Progress()
				if progress != last {
					last = progress
					if progress > 100 {
						progress = 100
					}
					fmt.Fprintf(os.Stdout, "\r%03d%%", progress)
				}
				if progress >= 100 {
					break
				}
			}

			fmt.Fprintf(os.Stdout, "\n")
			avg.Wait()
			break
		}

		os.Exit(0)

		break
	}

	/* check for -alias, -tags, -see, -attributes, -weight, -dir etc */

	hostname, _ := os.Hostname()

	args,err := arguments.Parse(os.Args,2)
	if err != nil {
		if err != arguments.ErrEmpty {
			fmt.Fprintf(os.Stderr,"error parsing arguments -- %v\n",err)
			os.Exit(1)
		}
	} 
		
	/* process for env */
	env := components.NewEnvironmentFromArgs(args)

	if env.Alias == "" {
		env.Alias = hostname
	}

	/* this is a argument debugger */
	if os.Args[1] == "arguments" {

		fmt.Printf("%s\n", env.PrettyPrint())

		/* FIXME: show a compact version */

		os.Exit(0)
	}

	component := lookup(os.Args[1])
	if component == nil {
		fmt.Fprintf(os.Stderr,"%q not found\n", os.Args[1])
		man()
		os.Exit(1)
	}

	others, err := Discovery(&network.Options{Local: true, Select: component.Name()})
	if err != nil {
		fmt.Fprintf(os.Stderr,"error with discovery -- %v\n", err)
		os.Exit(1)
	}

	internal.Lock()
	internal.component = component
	internal.environment = env
	internal.Unlock()

	flagged := false

	for _, node := range others {

		if node.Weight == env.Weight {
			flagged = true
		}
	}

	if flagged {

		fmt.Fprintf(os.Stderr,"component already running on local host with weight %d :\n", env.Weight)

		for _, node := range others {
			fmt.Fprintf(os.Stderr,"%s\n", node)
		}

		fmt.Fprintf(os.Stderr,"\nYou can run an additional %s component by using a different weight to the above\n", component.Name())

		os.Exit(3)
	}
	
	/* construct the work directory */

	/* TODO: move this to the supervisor code. determine how to go about setting up, if dir is not a valid directory then we 
   * should use a tmp directory -- although this will not useful for picking up 
   * where we left off. 
   */

	if env.Dir == "" { /* use the /tmp directory instead */
		env.Dir = path.Join(os.TempDir(),"mycelium")
	}

	if ! path.IsAbs(env.Dir) {
		fmt.Fprintf(os.Stderr,"dir %q not an absolute path but is required to be so\n",env.Dir)
		os.Exit(2)
	}

	identity := host.MakeIdentity(component.Name(), env.Weight)

	/* TODO : check the permissions are correct */
	if err := os.MkdirAll(path.Join(env.Dir,identity),0777); err != nil {
		fmt.Fprintf(os.Stderr,"unable to create directory %q -- %v\n", path.Join(env.Dir,identity),err)
		os.Exit(2)		
	}


	
	if Debug {
		fmt.Fprintf(os.Stdout,"working directory %q for identity %q\n",env.Dir,identity)
	}


	/* TODO: write this properly */
	portstr := fmt.Sprintf(":%d", env.Port)

	listener, err := net.Listen("tcp", portstr)
	if err != nil {
		fmt.Fprintf(os.Stderr,"listener error -- %v\n", err)
		os.Exit(1)
	}

	defer listener.Close()

	addr := listener.Addr().String()
	port, err := parseAddrPort(addr)
	if err != nil {
		fmt.Fprintf(os.Stderr,"error parsing addr port -- %v\n", err)
		os.Exit(1)
	}

	//fmt.Printf("http://localhost:%d\n", port)

	env.Port = port

	var srv http.Server

	idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		// We received an interrupt signal, shut down.
		if err := srv.Shutdown(context.Background()); err != nil {
			// Error from closing listeners, or context timeout:
			fmt.Fprintf(os.Stderr,"HTTP server Shutdown error -- %v\n", err)
		}
		close(idleConnsClosed)
	}()


	r := mux.NewRouter()
	r.HandleFunc("/", StatusHandler)

	srv.Handler = r

	/* create service */

	if err := component.Begin(identity,env); err != nil {
		fmt.Fprintf(os.Stderr,"error beginning service -- %v\n", err)
		os.Exit(1)
	}


	sr := r.PathPrefix(fmt.Sprintf("/%s", component.Name())).Subrouter()

	/* mount */
	if err := component.Mount(sr); err != nil {
		fmt.Fprintf(os.Stderr,"error mounting service -- %v\n", err)
		os.Exit(1)
	}

	/* loop -- read status from service and update the mDNS export as required */
	go func() {
		if err := srv.Serve(listener); err != http.ErrServerClosed {
			fmt.Fprintf(os.Stderr,"error serving -- %v\n", err)
		}
	}()

	running := true

	node := network.NewNode(identity, env.Alias, component.Name(), env.Port, env.Weight)

	for key, values := range env.Tags {

		node.SetTag(key, strings.Split(values, ";")...)
	}

	/* set the pid of the supervisor */
	node.SetTagv("pid",os.Getpid())


	for key, values := range env.Attributes {

		node.SetAttribute(key, strings.Split(values, ";")...)
	}

	/* generate trusted time stamp server */

	server, err := tts.NewServer(&tts.Configuration{Domain: fmt.Sprintf("%s.mycelium.local", node.Identity), Hash: "sha1"})
	if err != nil {
		fmt.Fprintf(os.Stderr,"error creating new trusted timestamp server -- %v\n", err)
		os.Exit(1)
	}

	/* TODO: the tts keys need storing for a later time when this node gets booted up again -- this could be stored in a .cache/mycelium/<uuid>.db file
   * with all the rest of the configuration */

	if err := server.GenerateKey(); err != nil {
		fmt.Fprintf(os.Stderr,"error generating server key -- %v\n", err)
		os.Exit(1)
	}

	internal.Lock()
	internal.server = server
	internal.Unlock()

	for {
		if !running {
			break
		}

		node = node.Update()

		hostinfo,err := host.GetInformation(host.LightProfile)
		if err != nil {
			fmt.Fprintf(os.Stderr,"error getting host information -- %v\n",err)
			os.Exit(2)
		}

		/* export information */
		if len(env.Dir) > 0 {
			node.SetInformation("dir",env.Dir)
		}
		if len(env.See) > 0 {
			node.SetInformation("see",env.See)
		}
		node.SetInformation("platform", hostinfo.Get("platform"))
		node.SetInformation("ram", hostinfo.Get("ram"))
		node.SetInformation("build-hash", build.Hash())
		node.SetInformation("git-hash", build.GitHash)
		/* == plugin */

		nodes, err := Discovery(&network.Options{Local: false, Select: strings.Join(registered(), ";")})
		if err != nil {
			fmt.Fprintf(os.Stderr,"error discovery -- %v\n", err)
			os.Exit(1) 
		}

		status := component.Status(nodes)

		node.SetInformation("status", fmt.Sprintf("%d", status))

		if err := Announce(node); err != nil {
			if err != network.ErrNoChange {
				/* we just log the error and move on, solves the no network available issue
				 * without having to restort to backoff code */
				fmt.Fprintf(os.Stderr,"error announcing -- %v, will continue onwards\n", err)
			}
		}

		select {
		case <-idleConnsClosed:
			running = false
			break
		case <-time.After(15 * time.Second):

			break
		}
	}

	/* unmount */
	if err := component.Unmount(); err != nil {
		fmt.Fprintf(os.Stderr,"error unmounting service -- %v\n", err)
		os.Exit(2)
	}

	/* end */
	if err := component.End(); err != nil {
		fmt.Fprintf(os.Stderr,"error ending service -- %v\n", err)
		os.Exit(2)
	}

}

type T struct {
	Identity string
	Host map[string]string
	Component string

	Env *components.Environment
	Build map[string]string

	Banner string
}

func tags(in []string) string {
	return strings.Join(in,",")
}

func render(w http.ResponseWriter, req *http.Request, fragment string, t T) {

	box,err := rice.FindBox("static")
	if err != nil {
		http.Error(w,err.Error(),501)
		return
	}

	body,err := box.String("templates.html")
	if err != nil {
		http.Error(w,err.Error(),501)
		return
	}

	m := template.FuncMap{"tags":tags}

	tmpl,err := template.New("~").Funcs(m).Parse(body)
	if err != nil {
		http.Error(w,err.Error(),501)
		return
	}

	if err := tmpl.ExecuteTemplate(w,fragment,t); err != nil {
		http.Error(w,err.Error(),501)
		return
	}
}


func StatusHandler(w http.ResponseWriter,req *http.Request) {
	internal.RLock()
	defer internal.RUnlock()

	identity := host.MakeIdentity(internal.component.Name(), internal.environment.Weight)

	banner,err := internal.server.ExportBanner()
	if err != nil {
		http.Error(w,err.Error(),501)
		return
	}

	info,err := host.GetInformation(host.LightProfile)
	if err != nil {
		http.Error(w,err.Error(),501)
		return
	}
	
	t := T{Identity: identity, Host: info, Component: internal.component.Name(), Env: internal.environment.Copy(), Banner: string(banner),Build: build.AsMap()}

	render(w,req,"status",t)
}


/* StatusHandler */
func OldStatusHandler(w http.ResponseWriter, req *http.Request) {
	internal.RLock()
	defer internal.RUnlock()


	banner, err := internal.server.ExportBanner()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	hostinfo,err := host.GetInformation(host.LightProfile)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/* TODO: switch to nice html template */

	w.Write([]byte("FUNGUS : MYCELIUM\n\n"))
	w.Write([]byte(fmt.Sprintf("running component %q\nstart-time %s up for %s\n\n", internal.component.Name(), internal.environment.StartTime, time.Since(internal.environment.StartTime))))

	w.Write([]byte("\nenvironment :-\n\n"))
	w.Write([]byte(fmt.Sprintf("Port: %d\nAlias: %q\nDir: %s\nSee: %s\nWeight: %d\n",
		internal.environment.Port, internal.environment.Alias, internal.environment.Dir,
		internal.environment.See, internal.environment.Weight)))
	w.Write([]byte("\ntags :-\n\n"))

	out := ""

	for key, values := range internal.environment.Tags {
		if len(out) > 0 {
			out += ", "
		}
		out += fmt.Sprintf("%s=%s", key, values)
	}

	w.Write([]byte(out))

	w.Write([]byte("\n\nattributes :-\n\n"))

	out = ""

	for key, values := range internal.environment.Attributes {
		if len(out) > 0 {
			out += ", "
		}
		out += fmt.Sprintf("%s=%s", key, values)
	}
	w.Write([]byte(out))

	w.Write([]byte("\n\nbuild :-\n\n"))
	w.Write([]byte(fmt.Sprintf("version %s\nbuild date %s\n", build.Version, build.Date)))
	w.Write([]byte(fmt.Sprintf("build-hash %s\n", build.Hash())))
	w.Write([]byte(fmt.Sprintf("git-hash %s\n\n", build.GitHash)))

	w.Write([]byte("\nhost :-\n\n"))
	w.Write([]byte(fmt.Sprintf("platform %s\n", hostinfo.Get("platform"))))
	w.Write([]byte(fmt.Sprintf("ram %s\n", humanize.SI(humanize.ComputeSI(float64(hostinfo.GetAsInt("ram",0)))))))

	w.Write([]byte("\n\n"))
	w.Write(banner)

	/* TODO: add url links to component index + admin points, such as /worknode/ + /worknode/admin */
}


