#!/bin/sh
user=$(whoami)
ver=$(cat VERSION)
date=$(date +%d-%b-%Y_%H:%M)
hash=$(git rev-parse HEAD)

echo "building linux/amd64..."
go build -ldflags "-s -X mae.earth/pkg/build.Version='${ver}' -X mae.earth/pkg/build.User='${user}' -X mae.earth/pkg/build.Date='${date}' -X mae.earth/pkg/build.GitHash=${hash}"


