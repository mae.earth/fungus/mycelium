package cache

import (
	"fmt"
	"errors"
	"time"
	"sync"
  "os"
	"path"

	"mae.earth/fungus/mycelium/ll/store/limit"
)

type CacheType string

func (c CacheType) String() string {
	return string(c)
}

var (
	ErrEmptyContent = errors.New("Empty Content")
	ErrEmptyKey = errors.New("Empty Key")
	ErrBadKey = errors.New("Bad Key")
	ErrNotFound = errors.New("Not Found")
	ErrEmptyNamespace = errors.New("Empty Namespace")
	ErrBadNamespace = errors.New("Bad Namespace")
	ErrMiscache = errors.New("Miscache")
)


const (
	Memory = CacheType("memory")
	File = CacheType("file")
)


/* Configuration */
type Configuration struct {
	Namespace string
	Type CacheType


	Opened time.Time
}

type Cache interface {

	Configuration() Configuration

	Write(string,[]byte) error
	Read(string) ([]byte,error)
	Delete(string) error
	
	Size() int

	Purge() error

	Close() error
}


type MemCache struct {
	mux sync.RWMutex

	records map[string][]byte

	namespace string

	opened time.Time
}

/* Configuration */
func (mcache *MemCache) Configuration() Configuration {
	return Configuration{Namespace: mcache.namespace,Type: Memory, Opened:mcache.opened}
}

/* Write */
func (mcache *MemCache) Write(key string, content []byte) error {
	if key == "" {
		return ErrEmptyKey
	}
	if content == nil {
		return ErrEmptyContent
	}

	mcache.mux.Lock()
	defer mcache.mux.Unlock()

	l := make([]byte,len(content))
	copy(l,content)

	mcache.records[key] = l

	return nil
}

/* Read */
func (mcache *MemCache) Read(key string) ([]byte,error) {
	if key == "" {
		return nil,ErrEmptyKey
	}
	mcache.mux.RLock()
	defer mcache.mux.RUnlock()

	if _,exists := mcache.records[key]; !exists {
		return nil,ErrMiscache
	}	

	out := make([]byte,len(mcache.records[key]))
	copy(out,mcache.records[key])

	return out,nil
}

/* Delete */
func (mcache *MemCache) Delete(key string) error {
	if key == "" {
		return ErrEmptyKey
	}
	mcache.mux.Lock()
	defer mcache.mux.Unlock()

	delete(mcache.records,key)

	return nil
}

/* Size */
func (mcache *MemCache) Size() int {
	mcache.mux.RLock()
	defer mcache.mux.RUnlock()

	return len(mcache.records)
}

/* Purge */
func (mcache *MemCache) Purge() error {
	mcache.mux.Lock()
	defer mcache.mux.Unlock()

	mcache.records = make(map[string][]byte,0)

	return nil
}

/* Close */
func (mcache *MemCache) Close() error {
	return mcache.Purge()
}


type FileCache struct {

	limitdb *limit.Limit
	
	namespace string
	opened time.Time
}

/* Configuration */
func (fcache *FileCache) Configuration() Configuration {
	return Configuration{Namespace: fcache.namespace,Type: File, Opened:fcache.opened}
}

/* Write */
func (fcache *FileCache) Write(key string, content []byte) error {
	if key == "" {
		return ErrEmptyKey
	}
	if content == nil {
		return ErrEmptyContent
	}
	return fcache.limitdb.Write(content,"soup",key)
}

/* Read */
func (fcache *FileCache) Read(key string) ([]byte,error) {
	if key == "" {
		return nil,ErrEmptyKey
	}

	content,err := fcache.limitdb.Read("soup",key)
	if err != nil {
		if err == limit.ErrNotFound {
			return nil,ErrMiscache
		}
	}
	return content,err
}

/* Delete */
func (fcache *FileCache) Delete(key string) error {
	if key == "" {
		return ErrEmptyKey
	}
	return fcache.limitdb.Delete("soup",key)
}

/* Size */
func (fcache *FileCache) Size() int {

	list,err := fcache.limitdb.Stat("soup")
	if err != nil {
		return -1
	}
	c := 0
	for _,r := range list {
		if !r.IsBucket {
			c ++
		}
	}

	return c
}

/* Purge */
func (fcache *FileCache) Purge() error {
	return fcache.limitdb.Delete("soup")
}


/* Close */
func (fcache *FileCache) Close() error {
	return fcache.limitdb.End()
}






/* New */
func New(p string, typeof CacheType,readonly bool) (Cache,error) {
	
	if p == "" {
		return nil,ErrEmptyNamespace
	}
	

	var cache Cache
	now := time.Now()

	switch typeof {
		case Memory:

			cache = &MemCache{namespace:p,opened:now,records:make(map[string][]byte,0)}

		break
		case File:

			fc := &FileCache{namespace:p,opened:now}
			
			if readonly {
				db,err := limit.BeginReadOnly(p)
				if err != nil {
					return nil,err
				}

				fc.limitdb = db
			} else {

				db,err := limit.Begin(p)
				if err != nil {
					return nil,err
				}

				fc.limitdb = db
			}
		
			cache = fc

		break
		default:

			return nil,fmt.Errorf("error, unknown cache type -- %q", typeof)
		break
	}


	return cache,nil
}

func NewUserFile(project,name string) (Cache,error) {

	if name == "" {
		return nil,ErrEmptyNamespace
	}
	
	p,err := os.UserCacheDir()
	if err != nil {
		return nil,err
	}

	actual := path.Join(p,project)

	os.MkdirAll(actual,0777)

	return New(path.Join(p,project,name),File,false)
}

func NewReadOnlyUserFile(project,name string) (Cache,error) {

	if name == "" {
		return nil,ErrEmptyNamespace
	}

	p,err := os.UserCacheDir()
	if err != nil {
		return nil,err
	}

	actual := path.Join(p,project)

	os.MkdirAll(actual,0777)

	return New(path.Join(p,project,name),File,true)
}






	
 
