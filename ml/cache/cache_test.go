package cache

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_Cache(t *testing.T) {
	Convey("Cache",t,func() {
		Convey("Memory",func() {
			Convey("empty namespace",func() {
				mcache,err := New("",Memory)
				So(err,ShouldEqual,ErrEmptyNamespace)
				So(mcache,ShouldBeNil)
			})

			Convey("basic usage",func() {		

				mcache,err := New("foo",Memory)
				So(err,ShouldBeNil)
				So(mcache,ShouldNotBeNil)

				conf := mcache.Configuration()
				So(conf.Type,ShouldEqual,Memory)
				So(conf.Namespace,ShouldEqual,"foo")
			
				So(mcache.Write("",[]byte("hello there")),ShouldEqual,ErrEmptyKey)
				So(mcache.Write("key",nil),ShouldEqual,ErrEmptyContent)
				So(mcache.Write("key",[]byte("hello there")),ShouldBeNil)
				So(mcache.Size(),ShouldEqual,1)
			
				c,err := mcache.Read("key")
				So(err,ShouldBeNil)
				So(string(c),ShouldEqual,"hello there")

				So(mcache.Delete(""),ShouldEqual,ErrEmptyKey)
				So(mcache.Delete("key"),ShouldBeNil)

				So(mcache.Size(),ShouldEqual,0)

				So(mcache.Close(),ShouldBeNil)				
			})
		})

		Convey("File",func() {
			Convey("empty namespace",func() {
				fcache,err := New("",File)
				So(err,ShouldEqual,ErrEmptyNamespace)
				So(fcache,ShouldBeNil)
			})
			Convey("basic usage",func() {

				fcache,err := New("./test.db",File)
				So(err,ShouldBeNil)
				So(fcache,ShouldNotBeNil)

				conf := fcache.Configuration()
				So(conf.Type,ShouldEqual,File)
				So(conf.Namespace,ShouldEqual,"./test.db")


				So(fcache.Write("",[]byte("hello there")),ShouldEqual,ErrEmptyKey)
				So(fcache.Write("key",nil),ShouldEqual,ErrEmptyContent)
				So(fcache.Write("key",[]byte("hello there")),ShouldBeNil)
				So(fcache.Size(),ShouldEqual,1)
			
				c,err := fcache.Read("key")
				So(err,ShouldBeNil)
				So(string(c),ShouldEqual,"hello there")

				So(fcache.Delete(""),ShouldEqual,ErrEmptyKey)
				So(fcache.Delete("key"),ShouldBeNil)

				So(fcache.Size(),ShouldEqual,0)
				


				So(fcache.Close(),ShouldBeNil)
			})
		})

	})
}
