
package bloom


import (
	"crypto/sha1"
	"crypto/md5"
	"crypto/sha256"
	"github.com/reusee/mmh3"
	"hash/fnv"

	"io"
	"fmt"	
)

const (
	MinimalFilterSize int = 2
	DefaultFilterSize int = 128
	MaximalFilterSize int = 1024 * 1024
	DoubleHashI int = 123
) 

//Dillinger and Manolios [8, 9], who suggested using the hash : g_i(x) = h_1(x) + ih_2(x) + i^2 mod m 

/* sizes: sha1 20; md5 16; sha256 32 */
func hash_org(key string,k int) []int {

	m := []int{-1,-2,-3}

	h0 := sha1.New()
	io.WriteString(h0,key)
	h := h0.Sum(nil)

	c := k
	for _,j := range h {
		c += int(j)
	}

	m[0] = c % k

	h1 := md5.New()
	io.WriteString(h1,key)
	h = h1.Sum(nil)		

	c = k
	for _,j := range h {
		c += int(j)
	}
	
	m[1] = c % k

	h2 := sha256.New()
	io.WriteString(h2,key)
	h = h2.Sum(nil)

	c = k
	for _,j := range h {
		c += int(j)
	}

	m[2] = c % k	

	return m
}

func hash_slower(key string,k int) []int {

	m := []int{k,k,k}

	hf0 := sha1.New()
	io.WriteString(hf0,key)
	h0 := hf0.Sum(nil)

	hf1 := md5.New()
	io.WriteString(hf1,key)
	h1 := hf1.Sum(nil)

	hf2 := sha256.New()
	io.WriteString(hf2,key)
	h2 := hf2.Sum(nil)
	
	for i := 0; i < 16; i++ {
		m[0] += int(h0[i])
		m[1] += int(h1[i])
		m[2] += int(h2[i])
	}

	for i := 16; i < 20; i++ {
		m[0] += int(h0[i])
		m[2] += int(h2[i])
	}

	for i := 20; i < 32; i++ {
		m[2] += int(h2[i])
	}

	m[0] = m[0] % k
	m[1] = m[1] % k
	m[2] = m[2] % k

	return m
}	

func hashUnfolded(key string,k int) []int {

	m := []int{k,k,k}

	h0 := fnv.New64()
	io.WriteString(h0,key)
	hash0 := h0.Sum(nil)
	for i := 0; i < len(hash0); i++ {
		m[0] += int(hash0[i])
	}

	m[0] = m[0] % k
	m[1] = doublehash(key,DoubleHashI) % k

	h1 := mmh3.New128()
	io.WriteString(h1,key)
	hash1 := h1.Sum(nil)
	for i := 0; i < len(hash1); i++ {
		m[2] += int(hash1[i])
	}
	m[2] = m[2] % k

	return m
} 

/* fnv len = 8, mmh3 len = 16 */
func hashUnfoldedSlow(key string,k int) []int {

	m := []int{k,k,k}

	h0 := fnv.New64()
	io.WriteString(h0,key)
	hash0 := h0.Sum(nil)
	for i := 0; i < len(hash0); i++ {
		m[0] += int(hash0[i])
	}

	h1 := mmh3.New128()
	io.WriteString(h1,key)
	hash1 := h1.Sum(nil)
	for i := 0; i < len(hash1); i++ {
		m[2] += int(hash1[i])
	}

	m[1] = m[0] + (DoubleHashI * m[2]) + (DoubleHashI * DoubleHashI)
	
	m[0] = m[0] % k
	m[1] = m[1] % k
	m[2] = m[2] % k
	return m
}

func hash(key string,k int) []int {

	m := []int{k,k,k}

	h0 := fnv.New64()
	io.WriteString(h0,key)
	hash0 := h0.Sum(nil)
	
	h1 := mmh3.New128()
	io.WriteString(h1,key)
	hash1 := h1.Sum(nil)

	m[0] += int(hash0[0])
	m[0] += int(hash0[1])
	m[0] += int(hash0[2])
	m[0] += int(hash0[3])
	m[0] += int(hash0[4])
	m[0] += int(hash0[5])
	m[0] += int(hash0[6])
	m[0] += int(hash0[7])

	m[2] += int(hash1[0])
	m[2] += int(hash1[1])
	m[2] += int(hash1[2])
	m[2] += int(hash1[3])
	m[2] += int(hash1[4])
	m[2] += int(hash1[5])
	m[2] += int(hash1[6])
	m[2] += int(hash1[7])	
	m[2] += int(hash1[8])
	m[2] += int(hash1[9])
	m[2] += int(hash1[10])
	m[2] += int(hash1[11])
	m[2] += int(hash1[12])
	m[2] += int(hash1[13])
	m[2] += int(hash1[14])
	m[2] += int(hash1[15])	

	m[1] = m[0] + (DoubleHashI * m[2]) + (DoubleHashI * DoubleHashI)

	m[0] = m[0] % k
	m[1] = m[1] % k
	m[2] = m[2] % k
	return m
}



//Dillinger and Manolios [8, 9], who suggested using the hash : g_i(x) = h_1(x) + ih_2(x) + i^2 mod m 
func doublehash(key string,i int) int {

	h0 := fnv.New64()
	io.WriteString(h0,key)
	hash0 := h0.Sum(nil)
	c0 := 0
	for i := 0; i < len(hash0); i++ {
		c0 += int(hash0[i])
	}

	h1 := mmh3.New128()
	io.WriteString(h1,key)
	hash1 := h1.Sum(nil)
	c1 := 0
	for i := 0; i < len(hash1); i++ {
		c1 += int(hash1[i])
	}

	return c0 + (i * c1) + (i * i)
}


type Stats struct {
	Size int
	Bytes int
	Count int
	Ratio float64
	Sparse float64
}

func (s Stats) String() string {
	return fmt.Sprintf("Bloom Filter -- size %d (%dbytes), keys %d, ratio %.2f, sparseness=%.2f",s.Size,s.Bytes,s.Count,s.Ratio,s.Sparse)
}

type Filter struct {
	bits []int	
	keys uint
}

func (f *Filter) String() string {
	return f.Stats().String()
}

func (f *Filter) Stats() Stats {
	if f.keys == 0 {
		return Stats{len(f.bits),(len(f.bits) * 4) + 4,int(f.keys),0.0,1.0}
	}

	zeros := 0
	for i := 0; i < len(f.bits); i++ {
		if f.bits[i] == 0 {
			zeros ++
		}
	}
	
	ratio := (float64(len(f.bits) - zeros) / 3) / float64(f.keys)
	sparse := (float64(zeros) / float64(len(f.bits)))

	return Stats{len(f.bits),(len(f.bits) * 4) + 4,int(f.keys),ratio,sparse}
}

func (f *Filter) Size() uint { return f.keys }

func (f *Filter) Len() int {
	return len(f.bits)
}

/* Append key to filter -- returning a new filter */
func (f *Filter) Append(key string) *Filter {

	/* hash(key) with 3 different crypto hashes : */
	m := hash(key,len(f.bits))

	nf := &Filter{}
	nf.keys = f.keys + 1
	nf.bits = make([]int,len(f.bits))
	copy(nf.bits,f.bits)

	for _,q := range m {
		nf.bits[q]++
	}

	return nf
}

func (f *Filter) AppendMultiple(keys ...string) *Filter {
	if len(keys) == 0 {
		return f
	}
	if len(keys) == 1 {
		return f.Append(keys[0])
	}

	nf := &Filter{}
	nf.bits = make([]int,len(f.bits))
	copy(nf.bits,f.bits)

	dups := make(map[string]bool,0)

	for _,key := range keys {
		if _,exists := dups[key]; exists {
			continue
		}

		m := hash(key,len(f.bits))
		for _,q := range m {
			nf.bits[q]++
		}
		
		dups[key] = true
	}
	nf.keys = f.keys + uint(len(dups))
	return nf
}


/* Remove key from filter -- returning a new filter */
func (f *Filter) Remove(key string) *Filter {
	if f.keys == 0 {
		return f
	}

	/* hash(key) with 3 different crypto hashes : */
	m := hash(key,len(f.bits))

	nf := &Filter{}
	nf.keys = f.keys - 1
	nf.bits = make([]int,len(f.bits))
	copy(nf.bits,f.bits)
	
	for _,q := range m {
		nf.bits[q]--
	}

	return nf
}

func (f *Filter) RemoveMultiple(keys ...string) *Filter {
	if f.keys == 0 || len(keys) == 0 {
		return f
	}
	if len(keys) == 1 {
		return f.Remove(keys[0])
	}		

	nf := &Filter{}
	nf.bits = make([]int,len(f.bits))
	copy(nf.bits,f.bits)

	dups := make(map[string]bool,0)

	for _,key := range keys {
		if _,exists := dups[key]; exists {
			continue
		}
		m := hash(key,len(f.bits))
		for _,q := range m {
			nf.bits[q]--
		}
		dups[key] = true
	}
	nf.keys = f.keys - uint(len(dups))
	return nf
}

/* Check key is in filter */
func (f *Filter) Check(key string) int {
	if f.keys == 0 {
		return 0
	}

	m := hash(key,len(f.bits))
	c := 0
	
	for _,q := range m {
		if f.bits[q] > 0 {
			c ++
		}
	}

	return c
}

/* Present -- is the key present? */
func (f *Filter) IsPresent(key string) bool {
	return (f.Check(key) == 3)
}

func (f *Filter) IsMultiplePresent(keys ...string) bool {
	if len(keys) == 0 {
		return false
	}
	if len(keys) == 1 {
		return f.IsPresent(keys[0])
	}
	dups := make(map[string]bool,0)
	for _,key := range keys {
		if _,exists := dups[key]; exists {
			continue
		}
		if f.Check(key) != 3 {
			return false
		}
		dups[key] = true
	}
	return true
} 



func NewBloomFilter(size int) *Filter {
	if size < MinimalFilterSize {
		size = MinimalFilterSize
	}
	if size > MaximalFilterSize {
		size = MaximalFilterSize
	}

	f := &Filter{}
	f.keys = 0
	f.bits = make([]int,size)
	return f
}

func PrintFilter(f *Filter) string {

	blocks := make([]string,0)
	blocks = append(blocks,f.String() + "\n\n")	
	l0 := ""
	l01 := ""
	l1 := ""
	columns := 0
	for i := 0; i < len(f.bits); i++ {
			if f.bits[i] > 0 {
				l0 += fmt.Sprintf("%03d|",i)
				l01 += fmt.Sprintf("---+")
				l1 += fmt.Sprintf("%03d ",f.bits[i])
				columns++
			}

			if columns >= 20 {
				columns = 0
				blocks = append(blocks,l0 + "\n" + l01 + "\n" +  l1 + "\n\n")
				l0 = ""
				l01 = ""
				l1 = ""
			}
	}
	if columns > 0 {
		blocks = append(blocks,l0 + "\n" + l01 + "\n" + l1 + "\n\n")
	}

	out := ""
	for _,b := range blocks {
		out += b
	}

	return out 
}






