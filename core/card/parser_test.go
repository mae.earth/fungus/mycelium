package card

import (
	"time"
	"fmt"

	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_CardParser(t *testing.T) {
	Convey("Datetime",t,func() {
		
		dt := time.Date(1999,time.March,30,1,2,3,0,time.UTC)
		
		out := dt.Format(time.RubyDate)
		So(out,ShouldNotBeEmpty)
			
		fmt.Printf("%q\n",out)


		dte,err := time.Parse(time.RubyDate,out)
		So(err,ShouldBeNil)
		So(dte,ShouldEqual,dt)	


	})

	Convey("Card Parser",t,func() {

		Convey("Attributes",func() {
			
			Convey("Int",func() {
			
				attr,err := A("prefix","title",45)
				So(err,ShouldBeNil)
				So(attr,ShouldNotBeNil)
				So(attr.Prefix,ShouldEqual,"prefix")
				So(attr.Name,ShouldEqual,"title")
				So(attr.Type,ShouldEqual,"int")
				So(attr.Value,ShouldNotBeNil)	

				So(attr.Int(-1),ShouldEqual,45)
				So(attr.Float(1.0),ShouldEqual,1.0)
				So(attr.String("foobar"),ShouldEqual,"foobar")
			})

			Convey("Float",func() {

				attr,err := A("prefix","number",1.23)
				So(err,ShouldBeNil)
				So(attr,ShouldNotBeNil)
				So(attr.Prefix,ShouldEqual,"prefix")
				So(attr.Name,ShouldEqual,"number")
				So(attr.Type,ShouldEqual,"float")
				So(attr.Value,ShouldNotBeNil)
				So(attr.Float(-1.0),ShouldEqual,1.23)
			})

			Convey("String",func() {

				attr,err := A("prefix","string","this is a string")
				So(err,ShouldBeNil)
				So(attr,ShouldNotBeNil)
				So(attr.Prefix,ShouldEqual,"prefix")
				So(attr.Name,ShouldEqual,"string")
				So(attr.Type,ShouldEqual,"string")
				So(attr.Value,ShouldNotBeNil)
				So(attr.String("foo"),ShouldEqual,"this is a string")
			})

			Convey("Boolean",func() {
	
				attr,err := A("prefix","boolean",true)
				So(err,ShouldBeNil)
				So(attr,ShouldNotBeNil)
				So(attr.Prefix,ShouldEqual,"prefix")
				So(attr.Name,ShouldEqual,"boolean")
				So(attr.Type,ShouldEqual,"boolean")
				So(attr.Value,ShouldNotBeNil)
				So(attr.Boolean(false),ShouldEqual,true)
			})

			Convey("Datetime",func() {

				now := time.Now()

				attr,err := A("prefix","time",now)
				So(err,ShouldBeNil)
				So(attr.Prefix,ShouldEqual,"prefix")
				So(attr.Name,ShouldEqual,"time")
				So(attr.Type,ShouldEqual,"datetime")
				So(attr.Value,ShouldNotBeNil)
				So(attr.Datetime(time.Now()),ShouldEqual,now)
			})

		})

		Convey("Form Card",func() {

			Convey("example 1",func() {
			
				card,err := Form("data","example 1", "title",45,"prefix float",1.2345,"prefix bool",true,"prefix string","hello there","prefix time",time.Now())
				So(err,ShouldBeNil)
				So(card,ShouldNotBeNil)
				So(len(card.Attributes),ShouldEqual,5)
	
				So(card.Attributes[0].Prefix,ShouldBeEmpty)
				So(card.Attributes[0].Name,ShouldEqual,"title")
				So(card.Attributes[0].Type,ShouldEqual,"int")
				So(card.Attributes[0].Value,ShouldNotBeNil)
	
				So(card.Attributes[1].Prefix,ShouldEqual,"prefix")
				So(card.Attributes[1].Name,ShouldEqual,"float")
				So(card.Attributes[1].Type,ShouldEqual,"float")
				So(card.Attributes[1].Value,ShouldNotBeNil)
	
				sexpr,err := Sexpr(card)
				So(err,ShouldBeNil)
				So(sexpr,ShouldNotBeEmpty)
	
				fmt.Printf("\n--\n%s\n--\n", string(sexpr))
			})

			Convey("example 2",func() {

				card,err := Form("data","example 2", "my titles delimited",[]string{"a","b","c"},"real",[]float64{0.1,0.2},"string","hello there")
				So(err,ShouldBeNil)
				So(card,ShouldNotBeNil)

				sexpr,err := Sexpr(card)
				So(err,ShouldBeNil)
				So(sexpr,ShouldNotBeEmpty)

				fmt.Printf("\n--\n%s\n--\n", string(sexpr))

				ncard,err := Parse(sexpr)
				So(err,ShouldBeNil)
				So(ncard,ShouldNotBeNil)

				So(ncard.Created,ShouldEqual,card.Created)

				So(ncard.Ident,ShouldEqual,card.Ident)
				So(ncard.Type,ShouldEqual,"data")
				So(ncard.Name,ShouldEqual,"example 2")
				So(ncard.Version,ShouldEqual,0)
			//	So(ncard.Created,ShouldEqual,card.Created)
			//	So(ncard.Modified,ShouldEqual,card.Modified)
				So(len(ncard.Attributes),ShouldEqual,len(card.Attributes))
				

			

				cl := card.VClock()
				So(cl,ShouldNotBeEmpty)
				fmt.Printf("\n--vclock\n%s\n--\n", cl)

				So(ncard.Attributes[0].Update([]string{"b","a","c"}),ShouldBeNil)

				sexpr,err = Sexpr(ncard)
				So(err,ShouldBeNil)
				So(sexpr,ShouldNotBeEmpty)

				fmt.Printf("\n--\n%s\n--\n", string(sexpr))


				cl2 := ncard.VClock()
				So(cl2,ShouldNotBeEmpty)
				fmt.Printf("relationship = %s\n",cl2.RelationshipTo(cl))

					

			})



		})
	})
}
