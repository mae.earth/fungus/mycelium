package card

import (
	"reflect"
	"fmt"
	"time"
	"errors"
	"strings"
	"strconv"
	"crypto/rand"
	"encoding/hex"	

	"mae.earth/pkg/vclock"
	"mae.earth/pkg/sexpr"
	"mae.earth/pkg/sexpr/cell"
)

var (
	ErrMismatchedParams = errors.New("Mismatched Parameters")
	ErrBadCard = errors.New("Bad Card")
	ErrBadValue = errors.New("Bad Value")
	ErrBadRoot = errors.New("Bad Root")

)


/* Attribute */
type Attribute struct {
	Prefix string 
	Name string
	Type string
	Modifier string /* used for specifics, such as delimited string */
	Version uint64
	Value interface{}	
}

func (a *Attribute) HasPrefix() bool {
	return (len(a.Prefix) > 0)
}

func (a *Attribute) HasModifier() bool {
	return (len(a.Modifier) > 0)
}

func (a *Attribute) Update(val interface{}) error {

	if reflect.TypeOf(a.Value) != reflect.TypeOf(val) {
		return fmt.Errorf("expecting value of type %q",reflect.TypeOf(a.Value))
	}

	a.Version ++
	a.Value = val

	return nil
}

/* Int */
func (a *Attribute) Int(def int) int {
 	if v,ok := a.Value.(int); ok {
		return v
	}
	return def
}

/* Float */
func (a *Attribute) Float(def float64) float64 {
	if v,ok := a.Value.(float64); ok {
		return v
	}
	return def
}

/* String */
func (a *Attribute) String(def string) string {
	if v,ok := a.Value.(string); ok {
		return v
	}
	return def
}

/* Boolean */
func (a *Attribute) Boolean(def bool) bool {
	if v,ok := a.Value.(bool); ok {
		return v
	}
	return def
}

/* Datetime */
func (a *Attribute) Datetime(def time.Time) time.Time {
	if v,ok := a.Value.(time.Time); ok {
		return v
	}
	return def
}
		



/* Card */
type Card struct {
	Ident string /* generated */
 	Type string
	Name string

	Version int

	Created time.Time
	Modified time.Time
	
	Attributes []*Attribute

	Id uint64 /* used for datastore access (bolt) */
}


/* Clock */
func (c *Card) VClock() vclock.Clock {

	cl := vclock.New()

	for _,attr := range c.Attributes {

		tag := vclock.NewTag(attr.Name,int(attr.Version))
		cl = cl.Update(tag)
	}
	return cl
}


/* Update */
func (c *Card) Update(name string, value interface{}) error {
	if value == nil {
		return ErrBadValue
	}

	for i := 0; i < len(c.Attributes); i++ {
		if c.Attributes[i].Prefix == "" && c.Attributes[i].Name == name {
			return c.Attributes[i].Update(value)
		}
	}
	return nil
}

/* UpdatePrefixed */
func (c *Card) UpdatePrefixed(prefix,name string,value interface{}) error {
	if value == nil {
		return ErrBadValue
	}

	for i := 0; i < len(c.Attributes); i++ {
		if c.Attributes[i].Prefix == prefix && c.Attributes[i].Name == name {
			return c.Attributes[i].Update(value)
		}
	}
	return nil
}

/* Get */
func (c *Card) Get(name string) interface{} {
	for i := 0; i < len(c.Attributes); i++ {
		if c.Attributes[i].Prefix == "" && c.Attributes[i].Name == name {
			return c.Attributes[i].Value
		}
	}
	return nil
}


/* Set */
func (c *Card) Set(name string, value *Attribute) *Card {
	if value == nil {
		return c
	}

	for i := 0; i < len(c.Attributes); i++ {
		if c.Attributes[i].Prefix == "" && c.Attributes[i].Name == name {
			/* replace */
			c.Attributes[i] = value
			return c
		}
	}
		
	c.Attributes = append(c.Attributes,value)

	return c
}

/* SetPrefixed */
func (c *Card) SetPrefixed(prefix,name string,value *Attribute) *Card {
	if value == nil {
		return c
	}

	for i := 0; i < len(c.Attributes); i++ {
		if c.Attributes[i].Prefix == prefix && c.Attributes[i].Name == name {
			c.Attributes[i] = value
			return c
		}
	}

	c.Attributes = append(c.Attributes,value)
	return c
}





/* A */
func A(prefix,name string,value interface{}) (*Attribute,error) {

	//fmt.Printf("A prefix=%q, name=%q, value=%+v\n",prefix,name,value)

	if value == nil {
		return nil,ErrBadValue
	}

	/* we have a limited set of types */
	switch reflect.TypeOf(value).String() {
		case "int":
			return &Attribute{Prefix:prefix,Name:name,Type:"int",Value:value},nil
		break
		case "[]int":
			return &Attribute{Prefix:prefix,Name:name,Type:"[]int",Value:value},nil
		break
		case "float64":
			return &Attribute{Prefix:prefix,Name:name,Type:"float",Value:value},nil
		break
		case "[]float64":
			return &Attribute{Prefix:prefix,Name:name,Type:"[]float",Value:value},nil
		break
		case "string":
			return &Attribute{Prefix:prefix,Name:name,Type:"string",Value:value},nil
		break
		case "[]string":
			return &Attribute{Prefix:prefix,Name:name,Type:"[]string",Value:value},nil
		break
		case "bool":
			return &Attribute{Prefix:prefix,Name:name,Type:"boolean",Value:value},nil
		break
		case "[]bool":
			return &Attribute{Prefix:prefix,Name:name,Type:"[]bool",Value:value},nil
		break
		case "time.Time":
			return &Attribute{Prefix:prefix,Name:name,Type:"datetime",Value:value},nil
		break
		default:
			return nil,fmt.Errorf("unknown type %q",reflect.TypeOf(value).String())
		break
	}

	return nil,nil
}

/* GenIdent */
func GenIdent() string {


	b := make([]byte,8)
	rand.Read(b)

	return hex.EncodeToString(b)
}

/* New */
func New(typeof,name string) *Card {

	now := time.Now().UTC()
	now,_ = time.Parse(time.RubyDate,now.Format(time.RubyDate))

	return &Card{Ident:GenIdent(), Type: typeof, Name: name, Version: 0, 
							 Created: now, Modified: now, Attributes: make([]*Attribute,0)}
}

/* Form */
func Form(typeof,name string,params ...interface{}) (*Card,error) {

	set := func(in,def string) string {
		if len(in) == 0 {
			return def
		}
		return in
	}

	c := New(typeof,name)

	if len(params) % 2 != 0 {
		return nil,ErrMismatchedParams
	}

	nattr := "--"

	for i := 0; i < len(params); i++ {
		if i % 2 == 0 {
			if v,ok := params[i].(string); !ok {
				return nil,fmt.Errorf("expecting string token at %d",i)
			} else {
				
				nattr = set(v,"--")
				if nattr == "--" {
					return nil,fmt.Errorf("string should not be empty at %d",i)
				}
			}
		} else {
			/* break into the prefix and name */
			parts := strings.Split(nattr," ")
			if len(parts) > 3 {
				return nil,fmt.Errorf("string splits to more than 3 parts at %d", i - 1)
			}


			if len(parts) == 1 {
				/* global */
				a,err := A("",parts[0],params[i])
				if err != nil {
					return nil,fmt.Errorf("bad value at %d -- %v",i,err)
				}			
				c.Attributes = append(c.Attributes,a)
			} else {
				
				a,err := A(parts[0],parts[1],params[i])
				if err != nil {
					return nil,fmt.Errorf("bad value at %d -- %v",i,err)
				}

				if len(parts) == 3 {
					a.Modifier = parts[2]
				}
		
				c.Attributes = append(c.Attributes,a)
			}
		}
	}

	return c,nil
}

type name string

func (n name) String() string {
	return string(n)
}

type lit string

func (l lit) String() string {
	return fmt.Sprintf("%q",string(l))
}


/* Sexpr */
func Sexpr(card *Card) ([]byte,error) {

	if card == nil {
		return nil,ErrBadCard
	}

	S := func(h cell.Stringer) *cell.Cell {
		return cell.New(h,nil)
	}
	cons := func(h,t *cell.Cell) *cell.Cell {
		return cell.New(h,t)
	}
		
	root := cell.New(name("card"),nil)
	root = cell.Append(root, S(name(card.Ident)))
	root = cell.Append(root, S(name(card.Type)))
	root = cell.Append(root, S(lit(card.Name)))
	root = cell.Append(root, S(name(fmt.Sprintf("%d",card.Version))))
	root = cell.Append(root, S(lit(card.Created.Format(time.RubyDate))))
	root = cell.Append(root, S(lit(card.Modified.Format(time.RubyDate))))

	attrs := cell.New(nil,nil)

	for _,attr := range card.Attributes {
		at := cell.New(name("a"),nil)

		at = cell.Append(at,S(name(fmt.Sprintf("%d",attr.Version))))
	
		if attr.HasPrefix() {
			at = cell.Append(at,S(lit(fmt.Sprintf("%s:%s",attr.Prefix,attr.Name))))
		} else {
			at = cell.Append(at,S(lit(attr.Name)))
		}

		if attr.HasModifier() {
			at = cell.Append(at,S(lit(attr.Modifier)))
		}

		switch attr.Type {
			case "int":
				d,ok := attr.Value.(int)
				if !ok {
					return nil,ErrBadValue
				}

				at = cell.Append(at,S(cell.List(name(attr.Type),name(fmt.Sprintf("%v",d)))))
			break

			case "[]int":
				da,ok := attr.Value.([]int)
				if !ok {
					return nil,ErrBadValue
				}

				l := cell.New(name("int-array"),nil)

				for _,v := range da {
					l = cell.Append(l,S(name(fmt.Sprintf("%v",v))))
				}

				at = cell.Append(at,cons(l,nil))
			break
			case "float":

				f,ok := attr.Value.(float64)
				if !ok {
					return nil,ErrBadValue
				}

				at = cell.Append(at,S(cell.List(name(attr.Type),name(f64(f)))))
			break
			case "[]float":
				fa,ok := attr.Value.([]float64)
				if !ok {
					return nil,ErrBadValue
				}

				l := cell.New(name("float-array"),nil)

				for _,v := range fa {
					l = cell.Append(l,S(name(f64(v))))
				}
		
				at = cell.Append(at,cons(l,nil))
			break
			case "string":

				str,ok := attr.Value.(string)
				if !ok {
					return nil,ErrBadValue
				}

				at = cell.Append(at,S(cell.List(name(attr.Type),lit(str))))
			break
			case "[]string":

				stra,ok := attr.Value.([]string)
				if !ok {
					return nil,ErrBadValue
				}

				l := cell.New(name("string-array"),nil)
	
				for _,v := range stra {
					l = cell.Append(l,S(lit(v)))
				}

				at = cell.Append(at,cons(l,nil))
			break
			case "datetime":

				dt,ok := attr.Value.(time.Time)
				if !ok {
					return nil,ErrBadValue
				}

				at = cell.Append(at,S(cell.List(name(attr.Type),lit(dt.Format(time.RubyDate)))))

			break
			case "boolean":

				b,ok := attr.Value.(bool)
				if !ok {
					return nil,ErrBadValue
				}

				at = cell.Append(at,S(cell.List(name(attr.Type),name(fmt.Sprintf("%v",b)))))
			break
			case "[]boolean":

				ba,ok := attr.Value.([]bool)
				if !ok {
					return nil,ErrBadValue
				}

				l := cell.New(name("boolean-array"),nil)
			
				for _,v := range ba {
					l = cell.Append(l,S(name(fmt.Sprintf("%v",v))))
				}

				at = cell.Append(at,S(l))
			break
			default:
				return nil,fmt.Errorf("unknown type %q",attr.Type)
			break
		}
	
		attrs = cell.Append(attrs,cell.Open(at))
	}

	out,err := sexpr.OutputString(cell.Open(cell.Append(root,S(attrs))))
	if err != nil {
		return nil,err
	}
	return []byte(out),nil
}

/* Parse */
func Parse(in []byte) (*Card,error) {

	strip := func(in string) string {
		if in[0] == '"' && in[len(in) - 1] == '"' {
			return in[1:len(in) - 1]
		}
		return in
	}



	root,err := sexpr.ParseString(string(in),nil)
	if err != nil {
		return nil,err
	}

	if root == nil {
		return nil,ErrBadRoot
	}

	if !root.IsList() {
		return nil,ErrBadRoot
	}

	c := root.List()

	if c == nil {
		return nil,ErrBadRoot
	}

	if val,ok := c.ToSValue(); ok {
		if val != "card" {
			return nil,ErrBadCard
		}
	} else {
		return nil,ErrBadRoot
	}

	/* identifier */
	c = c.Next()

	ident,ok := c.ToSValue()
	if !ok {
		return nil,ErrBadCard
	}

	/* type */
	c = c.Next()

	typeof,ok := c.ToSValue()
	if !ok {
		return nil,ErrBadCard
	}

	/* title */
	c = c.Next()

	title,ok := c.ToSValue()
	if !ok {
		return nil,ErrBadCard
	}

	/* version */
	c = c.Next()

	ver := -1

	if v,ok := c.ToSValue();  !ok {
		return nil,ErrBadCard
	} else {
		d,err := strconv.ParseInt(v,10,64)
		if err != nil {
			return nil,err
		}
		ver = int(d)
	}	

	/* created */
	c = c.Next()

	created := time.Now()

	if str,ok := c.ToSValue(); !ok {
		return nil,ErrBadCard
	} else {
		created,err = time.Parse(time.RubyDate,strip(str))
		if err != nil {
			return nil,err
		}
	}

	/* modified */
	c = c.Next()

	modified := time.Now()

	if str,ok := c.ToSValue();  !ok {
		return nil,ErrBadCard
	} else {
		modified,err = time.Parse(time.RubyDate,strip(str))
		if err != nil {
			return nil,err
		}
	}


	card := &Card{Ident:ident,
								Type:strip(typeof),
								Name:strip(title),
								Version:ver,
								Created:created,
								Modified:modified}

	card.Attributes = make([]*Attribute,0)


	/** attributes as a list */
	c = c.Next()
	if !c.IsList() {
		return nil,fmt.Errorf("expecting attributes list")
	}


	c = c.List()

	//fmt.Printf("%s\n",c)

	value := func(r *cell.Cell) interface{} {
		if r == nil {
			return nil
		}

		typeof := "--"
		var c interface{}

		i := 0
		for n := r.List(); n != nil; n = n.Next() {

			if n.IsList() {
				return nil
			}

			if i == 0 {

				typeof = n.SValue()
			//	fmt.Printf("type is %s\n",typeof)
				switch typeof {
					case "int":
						c = int(0)
					break
					case "int-array":
						c = []int{}
						typeof = "[]int"
					break
					case "float":
						c = float64(0.0)
					break
					case "float-array":
						c = []float64{}
						typeof = "[]float"
					break
					case "string":
						c = ""
					break
					case "string-array":
						c = []string{}
						typeof = "[]string"
					break
					case "boolean":
						c = false
					break
					case "boolean-array":
						c = []bool{}
						typeof = "[]boolean"
					break
					case "datetime":
						c = time.Now()
					break
					default:
						return nil
					break
				}
		

			} else {
				//fmt.Printf("value %d %q\n",i,n.SValue())
		
				switch typeof {
					case "int":
						d,err := strconv.ParseInt(n.SValue(),10,64)
						if err != nil {
							return nil
						}
						c = int(d)
					break
					case "[]int":
						d,err := strconv.ParseInt(n.SValue(),10,64)
						if err != nil {
							return nil
						}
						a := c.([]int)
						a = append(a,int(d))
						c = a	
					break
					case "float":
						f,err := strconv.ParseFloat(n.SValue(),64)
						if err != nil {
							return nil
						}	
						c = f
					break
					case "[]float":
						f,err := strconv.ParseFloat(n.SValue(),64)
						if err != nil {
							return nil
						}
						a := c.([]float64)
						a = append(a,f)
						c = a

					break
					case "string":
						c = strip(n.SValue())
					break
					case "[]string":
						a := c.([]string)
						a = append(a,strip(n.SValue()))
						c = a
					break
					case "boolean":
						if n.SValue() == "true" {
							c = true
						} else {
							c = false
						}
					break
					case "[]boolean":
						a := c.([]bool)
						if n.SValue() == "true" {
							a = append(a,true)
						} else {
							a = append(a,false)
						}
						c = a
					break
					case "datetime":

						dt,err := time.Parse(time.RubyDate,strip(n.SValue()))
						if err != nil {
							return nil
						}
						c = dt

					break
				}

			}

			i++
		}

		return c
	}

	for {

		if c == nil {
			break
		}

		ver := uint64(0)
		prefix := ""
		name := "--"
		mod := "--"

		if c.IsList() {
			i := 0
			for n := c.List(); n != nil; n = n.Next() { /* (a "prefix:title" ["modifier"] (type ...)) */

				if n.IsList() {
					attr,err := A(prefix,name,value(n))
					if err != nil {
						return nil,err
					}
					if mod != "--" {
						attr.Modifier = mod
					}
					attr.Version = ver

					card.Attributes = append(card.Attributes,attr)

				} else {
					if i == 0 && n.SValue() != "a" {
						return nil,ErrBadValue
					}
					if i == 1 {
						d,err := strconv.ParseInt(n.SValue(),10,64)
						if err != nil {
							return nil,err
						}
						ver = uint64(d)
					}
					if i == 2 {
						name = strip(n.SValue())
						if strings.Contains(name,":") {
							parts := strings.Split(name,":")
							prefix = parts[0]
							name = parts[1]
						}
					}
					if i == 3 {
						mod = strip(n.SValue())
					}
				}
				i++
			}
		}
		c = c.Next()
	}

	return card,nil
}








