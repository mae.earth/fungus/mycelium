/* mae.earth/pkg/misc/sleeve/sleeve_test.go */
package sleeve

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"compress/lzw"
	"bytes"
	"io"
	"strings"
	"encoding/json"
	"github.com/vmihailenco/msgpack"

	"mae.earth/pkg/card"
)

func Test_Sleeve(t *testing.T) {

	Convey("Sleeve", t, func() {

		sleeve := New("meta","hidden",nil)
		So(sleeve,ShouldNotBeNil)
		So(sleeve.ContentType, ShouldEqual,"meta")
		So(sleeve.ContentDisposition, ShouldEqual, "hidden")
		So(sleeve.Version, ShouldEqual, 0)
		So(sleeve.InternalVersion, ShouldEqual, CurrentInternalVersion)
		So(sleeve.Content, ShouldBeNil)

		content,err := sleeve.Encode()
		So(err,ShouldBeNil)
		So(content,ShouldNotBeEmpty)

		Convey("tags",func() {
	
			sleeve.Tags.Set("foo","bar","sid")
			So(sleeve.Tags.Size(),ShouldEqual,1)

			tags := sleeve.Tags.Get("foo")
			So(tags,ShouldNotBeNil)
			So(len(tags),ShouldEqual,2)
			So(strings.Join(tags," "),ShouldEqual,"bar sid")

			sleeve.Tags.Remove("foo")
			So(sleeve.Tags.Size(),ShouldEqual,0)

			sleeve.Tags.Append("foo","woo","soo","hoo")
			So(sleeve.Tags.Size(),ShouldEqual,0)

			sleeve.Tags.Set("foo","bar").Append("foo","woo","soo","hoo")
			So(sleeve.Tags.Size(),ShouldEqual,1)

			tags = sleeve.Tags.Get("foo")
			So(tags,ShouldNotBeNil)
			So(len(tags),ShouldEqual,4)
			So(strings.Join(tags," "),ShouldEqual,"bar hoo soo woo")
			

		})

		Convey("card",func() {

			test := `(card data 88745ec816d24fc5 5 (A (a "Alice" (string "in wonderland")) (a "List of Things" (delimited-string "," "Butterfly,Rabbit,Hamster")) (a "time" (time "11:39")) (A "foo" (a "bar" (string "foobar")))))`
	
			sleeve = sleeve.CopyWithContent([]byte(test))
			So(sleeve,ShouldNotBeNil)
			So(len(sleeve.Content),ShouldEqual,len(test))
		
		})

	})

	Convey("Reference",t,func() {

		ref := NewReference("/foo/bar",nil)
		So(ref,ShouldNotBeNil)
		So(ref.ContentType,ShouldEqual,TypeReference)
		So(ref.ContentDisposition,ShouldEqual,TypeScript)
		So(ref.Version,ShouldEqual,0)
		So(ref.InternalVersion,ShouldEqual,CurrentInternalVersion)
		So(ref.Content,ShouldBeNil)

		content,err := ref.Encode()
		So(err,ShouldBeNil)
		So(content,ShouldNotBeEmpty)
	})
}


func Benchmark_JsonMarshal(b *testing.B) {

	b.Skip()

	sleeve := New("meta","hidden",[]byte("hello there"))

	for i := 0; i < b.N; i++ {

		json.Marshal(sleeve)
	}
}

func Benchmark_JsonUnmarshal(b *testing.B) {

	b.Skip()

	sleeve := New("meta","hidden",[]byte("hello there"))
	content,err := json.Marshal(sleeve)
	if err != nil {
		b.Error(err)
	}

	for i := 0; i < b.N; i++ {

		var s *Sleeve
		json.Unmarshal(content,&s)
	}
}


func Benchmark_MsgpackMarshal(b *testing.B) {

	b.Skip()
	
	sleeve := New("meta","hidden",[]byte("hello there"))

	for i := 0; i < b.N; i++ {

		msgpack.Marshal(sleeve)
	}
}

func Benchmark_MsgpackUnmarshal(b *testing.B) {
	
	b.Skip()

	sleeve := New("meta","hidden",[]byte("hello there"))
	content,err := msgpack.Marshal(sleeve)
	if err != nil {
		b.Error(err)
	}

	for i := 0; i < b.N; i++ {

		var s *Sleeve
		msgpack.Unmarshal(content,&s)
	}
}


func Benchmark_CustomMarshal(b *testing.B) {

	sleeve := New("meta","hidden",[]byte("hello there"))

	for i := 0; i < b.N; i++ {

		sleeve.Encode()
	}
}

func Benchmark_CustomUnmarshal(b *testing.B) {

	b.StopTimer()

	sleeve := New("meta","hidden",[]byte("hello there"))
	content,err := sleeve.Encode()
	if err != nil {
		b.Error(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		ToSleeve(content)
	}
}

func Benchmark_CustomGetContentWChecksum(b *testing.B) {

	b.StopTimer()

	sleeve := New("meta","hidden",[]byte("hello there"))
	content,err := sleeve.Encode()
	if err != nil {
		b.Error(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		contenttype,_,err := GetContent(content)
		if err != nil {
			b.Error(err)
		}
		if contenttype != "meta" {
			b.Errorf("bad content")
		}
	}
}

func Benchmark_CustomGetContent(b *testing.B) {

	b.StopTimer()

	sleeve := New("meta","hidden",[]byte("hello there"))
	content,err := sleeve.Encode()
	if err != nil {
		b.Error(err)
	}

	b.StartTimer()	

	for i := 0; i < b.N; i++ {

		contenttype,_,err := GetContentWithoutChecksum(content)
		if err != nil {
			b.Error(err)
		}
		if contenttype != "meta" {
			b.Errorf("bad content")
		}
	}
}

func Benchmark_CustomGetContentCardText(b *testing.B) {

	b.StopTimer()	
	
	c,err := card.New("")
	if err != nil {
		panic(err)
	}
	c.Set("Alice",card.String("in wonderland"))
	c.Set("List of Things",card.DelimitedString{",","Butterfly,Rabbit,Hamster"})
	c.Set("Id",card.Byte(101))
	c.Set("Number #",card.Number(405))
	c.Set("Pi",card.Real(3.415))
	c.Set("datetime",card.DatetimeNow())	
	
	test,err := card.Output(c)
	if err != nil {
		b.Error(err)
	}
	
	sleeve := New("card","text",[]byte(test))
	content,err := sleeve.Encode()
	if err != nil {
		b.Error(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		contenttype,card_content,err := GetContentWithoutChecksum(content)
		if err != nil {
			b.Error(err)
		}
		if contenttype != "card" {
			b.Errorf("bad content")
		}

		if _,err := card.Parse(string(card_content));  err != nil {
			b.Error(err)
		}		
	}
}


func Benchmark_CustomGetContentCardTextLZW(b *testing.B) {

	b.StopTimer()	
	
	c,err := card.New("")
	if err != nil {
		panic(err)
	}
	c.Set("Alice",card.String("in wonderland"))
	c.Set("List of Things",card.DelimitedString{",","Butterfly,Rabbit,Hamster"})
	c.Set("Id",card.Byte(101))
	c.Set("Number #",card.Number(405))
	c.Set("Pi",card.Real(3.415))
	c.Set("datetime",card.DatetimeNow())	
	
	test,err := card.Output(c)
	if err != nil {
		b.Error(err)
	}

	buf := bytes.NewBuffer(nil)
	w := lzw.NewWriter(buf,lzw.LSB,8)
	io.WriteString(w,test)
	w.Close()
	
	sleeve := New("card","text; lzw",buf.Bytes())
	content,err := sleeve.Encode()
	if err != nil {
		b.Error(err)
	}

	b.StartTimer()

	classic := make([]byte,512)

	for i := 0; i < b.N; i++ {

		contenttype,card_content,err := GetContentWithoutChecksum(content)
		if err != nil {
			b.Error(err)
		}
		if contenttype != "card" {
			b.Errorf("bad content")
		}

		buf := bytes.NewBuffer(card_content)
		r := lzw.NewReader(buf,lzw.LSB,8)
		n,err := r.Read(classic)
		if err != nil {
			b.Error(err)
		}
		
		if _,err := card.Parse(string(classic[:n]));  err != nil {
			b.Error(err)
		}		
	}
}








