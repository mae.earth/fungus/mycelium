/* mae.earth/pkg/misc/kv/kv.go */
package kv

import (
	"sync"
	"fmt"
	"sort"
	"strings"
	"strconv"
	"crypto/sha512"
	"encoding/hex"
)


func f64(f float64) string {

	str := fmt.Sprintf("%f", f)
	s := 0
	neg := false
	for i, c := range str {
		if c != '0' {
			if c == '-' {
				neg = true
				continue
			}
			s = i
			break
		}
		if c == '.' {
			break
		}
	}

	e := 0
	for i := len(str) - 1; i >= 0; i-- {
		if str[i] != '0' {
			e = i + 1
			break
		}
		if str[i] == '.' {
			break
		}
	}

	str = str[s:e]
	if str == "." {
		str = "0"
	}
	if neg {
		str = "-" + str
	}
	if str[len(str)-1] == '.' {
		str = str[:len(str)-1]
	}

	return str
}

/* Store */
type Store struct {
	mux sync.RWMutex
	values map[string]string
}

/* Size */
func (s *Store) Size() int {
	s.mux.RLock()
	defer s.mux.RUnlock()

	return len(s.values)
}

/* Hash */
func (s *Store) Hash() string {
	s.mux.RLock()
	defer s.mux.RUnlock()

	var out []string

	for name,values := range s.values {
		out = append(out,fmt.Sprintf("%s=%s",name,values))
	}

	sort.Slice(out,func(i,j int) bool { return out[i] < out[j] })

	h := sha512.New()
	
	for _,part := range out {
		h.Write([]byte(part))
	}

	return hex.EncodeToString(h.Sum(nil))
}



/* Set */
func (s *Store) Set(name string, values ...interface{}) bool {
	s.mux.Lock()
	defer s.mux.Unlock()

	if len(name) == 0 {
		return false
	}

	if len(values) == 0 {
		/* delete mode */
		if _,exists := s.values[ name ]; !exists {
			return false
		}

		delete(s.values, name)

		return true
	}

	entries := []string{}

	for _,value := range values {
		
		if av,ok := value.(int64); ok {
			entries = append(entries,fmt.Sprintf("%d",av))
		} else if av,ok := value.(float64); ok {
			entries = append(entries,f64(av))
		} else if av,ok := value.(bool); ok {
			if av {
				entries = append(entries,"yes")
			} else {
				entries = append(entries,"no")
			}
		} else {
			entries = append(entries,fmt.Sprintf("%s",value))
		}
	}

	s.values[ name ] = strings.Join(entries,";")

	return true
}

/* Get */
func (s *Store) Get(name string) ([]string,bool) {
	s.mux.RLock()
	defer s.mux.RUnlock()

	v,exists := s.values[ name ]
	if !exists {
		return []string{},false
	}

	return strings.Split(v,";"),exists
}

/* String */
func (s *Store) String(name string,def string) string {
	s.mux.RLock()
	defer s.mux.RUnlock()

	out := def 

	if v,exists := s.values[ name ]; exists {
		for _,part := range strings.Split(v,";") {
			switch part[0] {
				case '.','0','-','+','1','2','3','4','5','6','7','8','9':
					continue
				break
			}
			if part == "yes" || part == "no" {
				continue
			}
			out = part
		}
	}

	return out
}

/* Int64 */
func (s *Store) Int64(name string,def int64) int64 {
	s.mux.RLock()
	defer s.mux.RUnlock()

	out := def

	if v,exists := s.values[ name ]; exists {
		
		for _,part := range strings.Split(v,";") {
			d,err := strconv.ParseInt(part,10,64)
			if err != nil {
				continue
			}
			out = d
		}
	}

	return out
}

/* Float64 */
func (s *Store) Float64(name string,def float64) float64 {
	s.mux.RLock()
	defer s.mux.RUnlock()

	out := def

	if v,exists := s.values[ name ]; exists {
		for _,part := range strings.Split(v,";") {
			f,err := strconv.ParseFloat(part,64)
			if err != nil {
				continue
			}
			out = f
		}
	}

	return out
}

/* Bool */
func (s *Store) Bool(name string,def bool) bool {
	s.mux.RLock()
	defer s.mux.RUnlock()

	out := def

	if v,exists := s.values[ name ]; exists {
		for _,part := range strings.Split(v,";") {
			if part == "yes" {
				out = true
			} else if part == "no" {
				out = false
			}
		}
	}			

	return out
}

/* Raw */
func (s *Store) Raw() map[string]string {
	s.mux.RLock()
	defer s.mux.RUnlock()

	out := make(map[string]string,len(s.values))
	
	for k,v := range s.values {
		out[k] = v
	}
	return out
}

/* Wrap */
func Wrap(values map[string]string) *Store {
	store := &Store{}
	store.values = values
	return store
}


/* New */
func New() *Store {

	store := &Store{}
	store.values = make(map[string]string,0)

	return store
}



