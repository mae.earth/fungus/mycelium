package ui

import (
	"time"
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_Bar(t *testing.T) {

	Convey("Box",t,func() {

		b := Box("test for the win",100)
		So(b,ShouldNotBeNil)

		for i := 0; i < 100; i ++ {
			time.Sleep(10 * time.Millisecond)
			b("test",i + 1)
		}

		for i := 0; i < 100; i ++ {
			time.Sleep(1 * time.Millisecond)
			b("foo man",i + 1)
		}
	})

	Convey("Box large",t,func() {

		b := Box("test",1201)
		So(b,ShouldNotBeNil)

		
		for i := 0; i < 1201; i++ {	
			time.Sleep(2 * time.Millisecond)
			b("test",i + 1)
		}
	})
}



