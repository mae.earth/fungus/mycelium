package ui

import (
	"os"
	"fmt"
	"time"
	"strings"
	"unicode/utf8"
)

const (
	ConsoleWidth int = 85
)

/* Box */
func Box(maxtitle string, total int) func(string,int) {
	
	dsize := fmt.Sprintf("%d",total)
	dsize = fmt.Sprintf("%%0%dd", len(dsize))

	msize := fmt.Sprintf("%%%ds",utf8.RuneCountInString(maxtitle))

	format := "\r" + msize + " " + dsize + "/" + dsize + "[ %s|%03.0f%% ] %s"

	sample := fmt.Sprintf(format,
												strings.Repeat("x",utf8.RuneCountInString(maxtitle)),
												0,
												total,
												"",
												100.0,
												time.Since(time.Now()))

	bar := strings.Repeat("#", ConsoleWidth - utf8.RuneCountInString(sample))

	idx := float64(len(bar)) / 100.0

	started := time.Now()

	f := func(label string,current int) {

		per := (float64(current) / float64(total)) * 100.0
		at := int(idx * per)

		fmt.Fprintf(os.Stdout,format,label,current,total,bar[:at],per,time.Since(started))
		
		if current == total {
			fmt.Fprintf(os.Stdout,"\n")
		}
	}
	return f
}




