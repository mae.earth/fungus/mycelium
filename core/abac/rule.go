/* fungus/mycelium/core/abac/rule.go
 * mae 012018
 */
package abac

import (
	"fmt"
	"strings"

	"mae.earth/fungus/mycelium/core/kv"
	"mae.earth/pkg/sexpr"
	"mae.earth/pkg/sexpr/cell"
)

/* Rule */
type Rule interface {
	Eval(Subject, Object, Environment) Set
	Sexpr() *cell.Cell
	Encode() (string, error)
	Description() string
	Label() string
	New(*kv.Store, Set) Rule
}

/* MustMatch */
type MustMatch struct {
	description string

	subject string
	object  string

	set Set
}

func (m MustMatch) Label() string {
	return "MustMatch"
}

func (m MustMatch) Description() string {
	return m.description
}

/* Eval */
func (m MustMatch) Eval(subject Subject, object Object, env Environment) Set {

	if subject == nil || object == nil {
		return FalseSet
	}

	parts := strings.Split(m.subject, "=")
	if len(parts) != 2 {
		return FalseSet
	}

	skey := parts[0]
	sval := parts[1]

	parts = strings.Split(m.object, "=")
	if len(parts) != 2 {
		return FalseSet
	}

	okey := parts[0]
	oval := parts[1]

	found := false

	for _, value := range subject.GetAttribute(skey, []string{}) {
		if value == sval {
			found = true
			break
		}
	}

	if !found {
		return FalseSet
	}

	found = false

	for _, value := range object.GetAttribute(okey, []string{}) {
		if value == oval {
			found = true
			break
		}
	}

	if !found {
		return FalseSet
	}

	return m.set
}

func (m MustMatch) Sexpr() *cell.Cell {
	return cell.New(cell.List(v("abac-policy-rule"), l("must-match"),
		cell.Cons(v("subject"), l(m.subject)),
		cell.Cons(v("object"), l(m.object)),
		cell.Cons(v("description"), l(m.description)),
		m.set.Sexpr()), nil)
}

/* Encode */
func (m MustMatch) Encode() (string, error) {

	return sexpr.OutputString(m.Sexpr())
}

func (m MustMatch) New(store *kv.Store, set Set) Rule {

	subject := store.String("subject", "")
	object := store.String("object", "")
	description := store.String("description", "")

	return MustMatch{description: description, subject: subject, object: object, set: set}
}

func MustMatchRule(subjectk, subjectv, objectk, objectv string, set Set, description string) MustMatch {

	return MustMatch{description: description,
		subject: fmt.Sprintf("%s=%s", subjectk, subjectv),
		object:  fmt.Sprintf("%s=%s", objectk, objectv),
		set:     set}
}

/* DecodeRule -- TODO: move Rules to seperate package(s) and add a register function */
func DecodeRule(root *cell.Cell) Rule {
	if root == nil || !root.IsList() {

		return nil
	}

	root = root.List()
	head := cell.First(root)

	if head.SValue() != "abac-policy-rule" {

		return nil
	}

	/*(abac-policy-rule "must-match" (subject "user=Alice")(object "type=normal")(abac-set (read no)(write no)(create no)(delete no)(copy no)(rename no)(execute no))*/

	rest := cell.Rest(root)
	head = cell.First(rest)

	if head.IsList() {

		return nil
	}

	store := kv.New()
	name := "-"
	rset := FalseSet
	rules := make(map[string]Rule, 0)
	rules["must-match"] = MustMatch{}

	/* name of rule */
	switch strings.ToLower(head.SValue()) {
	case "\"must-match\"":
		name = "must-match"
		store.Set("subject", "-")
		store.Set("object", "-")

		break
	default:

		return nil
	}

	rest = rest.Next()

	for {
		if rest == nil {
			break
		}

		if rest.IsList() {
			list := rest.List()
			lhead := cell.First(list)

			if lhead.IsList() {

				return nil
			}

			if lhead.SValue() == "abac-set" {
				rset = rset.Decode(list)
			} else {
				/* should be an attribute pair */
				lrest := cell.Rest(list)
				if !lrest.IsList() {
					store.Set(lhead.SValue(), lrest.SValue())
				}

				/* TODO: could be a list of strings */

			}

		} else {

			//fmt.Printf("\t%q\n",rest.SValue())
		}
		rest = rest.Next()
	}

	r, ok := rules[name]
	if !ok {
		return nil
	}

	return r.New(store, rset)
}

/* DecodePolicy */
func DecodePolicy(root *cell.Cell) Policy {

	/*	 (abac-policy ((abac-policy-rule "must-match" (subject "user=Alice")(object "type=normal")(description "block Alice")(abac-set (read no)(write no)(create no)(delete no)(copy no)(rename no)(execute no)))(abac-policy-rule "must-match" (subject "user=Fred")(object "type=normal")(description "allow Fred")(abac-set (read yes)(write yes)(create yes)(delete no)(copy no)(rename no)(execute no)))))
	 */

	if root == nil || !root.IsList() {
		return nil
	}

	root = root.List()
	head := cell.First(root)

	if head.SValue() != "abac-policy" {
		return nil
	}

	rest := cell.Rest(root)

	if !rest.IsList() {
		return nil
	}

	list := rest.List()

	rules := make([]Rule, 0)

	for {
		if list == nil {
			break
		}

		if list.IsList() {

			ilist := list.List()
			ihead := cell.First(ilist)

			if ihead.SValue() == "abac-policy-rule" {
				if rule := DecodeRule(list); rule != nil {

					rules = append(rules, rule)
				}
			}
		}
		list = list.Next()
	}

	return rules
}

func Wrap(root *cell.Cell) *cell.Cell {
	if root == nil {
		return nil
	}
	if root.IsList() {
		return root
	}

	return cell.List(root)
}
