/* assemblage/core/abac/abac_test.go
 * mae 12017
 */
package abac

import (
	"fmt"
	"strings"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

type test map[string]string

func (t test) GetAttribute(name string, def []string) []string {
	if v, ok := t[name]; ok {
		return strings.Split(v, ";")
	}
	return def
}

func Test_Set(t *testing.T) {
	Convey("Set", t, func() {

		So(FalseSet.Read(), ShouldBeFalse)
		So(FalseSet.String(), ShouldEqual, "r:n,w:n,c:n,d:n,cp:n,rn:n,x:n")

		fmt.Printf("set %q\n", FalseSet.String())

		str, err := FalseSet.Encode()
		So(err, ShouldBeNil)
		fmt.Printf("set %q\n", str)

		So(Set{true, true, true, true, true, true, true}.String(), ShouldEqual, "r:y,w:y,c:y,d:y,cp:y,rn:y,x:y")

		spr := Wrap(FalseSet.Sexpr())
		So(spr, ShouldNotBeNil)
		s2 := FalseSet.Decode(spr)
		So(s2.String(), ShouldEqual, "r:n,w:n,c:n,d:n,cp:n,rn:n,x:n")

		spr = Wrap(Set{true, false, false, false, false, false, false}.Sexpr())
		So(spr, ShouldNotBeNil)
		s2 = FalseSet.Decode(spr)
		So(s2.String(), ShouldEqual, "r:y,w:n,c:n,d:n,cp:n,rn:n,x:n")

	})

	Convey("Rule", t, func() {

		m := MustMatchRule("user", "Alice", "type", "normal", FalseSet, "block Alice")
		str, err := m.Encode()
		So(err, ShouldBeNil)

		fmt.Printf("rule %s\n", str)

		spr := m.Sexpr()
		So(spr, ShouldNotBeNil)

		m1 := DecodeRule(spr)
		So(m1, ShouldNotBeNil)

	})

	Convey("Policy", t, func() {

		m := MustMatchRule("user", "Alice", "type", "normal", FalseSet, "block Alice")
		m1 := MustMatchRule("user", "Fred", "type", "normal", Set{true, true, true, false, false, false, false}, "allow Fred")

		str, err := Policy([]Rule{m, m1}).Encode()
		So(err, ShouldBeNil)

		fmt.Printf("policy %s\n", str)

		subject := test(make(map[string]string, 0))
		subject["name"] = "Alice"

		object := test(make(map[string]string, 0))
		object["type"] = "normal"

		b := new(GrantBuilder)
		b.WithSubject(subject).WithObject(object).WithAction(READ).WithEnvironment()
		So(b.Validate(), ShouldBeNil)

		So(Policy([]Rule{m, m1}).Grant(b), ShouldBeNil)
		So(b.HaveGrant(), ShouldBeFalse)
		So(b.Audit(), ShouldNotBeEmpty)

		fmt.Printf("audit %q\n", b.Audit())

		spr := Policy([]Rule{m, m1}).Sexpr()

		p1 := DecodePolicy(spr)
		So(p1, ShouldNotBeNil)
		So(len(p1), ShouldEqual, 2)

	})
}

func Benchmark_SetEncodeSexpr(b *testing.B) {

	for i := 0; i < b.N; i++ {

		FalseSet.Sexpr()
	}
}

func Benchmark_SetDecodeSexpr(b *testing.B) {

	b.StopTimer()

	spr := Wrap(FalseSet.Sexpr())

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		FalseSet.Decode(spr)
	}
}

func Benchmark_RuleEncodeSexpr(b *testing.B) {

	for i := 0; i < b.N; i++ {

		MustMatchRule("user", "Alice", "type", "normal", FalseSet, "test").Sexpr()
	}
}

func Benchmark_RuleDecodeSexpr(b *testing.B) {

	b.StopTimer()

	spr := MustMatchRule("user", "Alice", "type", "normal", FalseSet, "test").Sexpr()

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		DecodeRule(spr)
	}
}

func Benchmark_PolicyEncodeSexpr(b *testing.B) {

	b.StopTimer()

	m := MustMatchRule("user", "Alice", "type", "normal", FalseSet, "block Alice")
	m1 := MustMatchRule("user", "Fred", "type", "normal", Set{true, true, true, false, false, false, false}, "allow Fred")

	p := Policy([]Rule{m, m1})

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		p.Sexpr()
	}

}

func Benchmark_PolicyDecodeSexpr(b *testing.B) {

	b.StopTimer()

	m := MustMatchRule("user", "Alice", "type", "normal", FalseSet, "block Alice")
	m1 := MustMatchRule("user", "Fred", "type", "normal", Set{true, true, true, false, false, false, false}, "allow Fred")

	spr := Policy([]Rule{m, m1}).Sexpr()

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		DecodePolicy(spr)
	}
}
