/* fungus/mycelium/core/abac/abac.go
 * mae 012018
 */
package abac

import (
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"fmt"
	"strings"
	"time"

	"mae.earth/pkg/sexpr"
	"mae.earth/pkg/sexpr/cell"
)

var (
	ErrInvalidGrantBuilder    = errors.New("invalid grant builder")
	ErrGrantBuilderAlreadyRun = errors.New("already run grant builder")
)

const (
	ContentType  string = "text/sexpr"
	ContentDispo string = "abac/policy"
)

/* Action */
type Action string

const (
	READ    = Action("read")
	WRITE   = Action("write")
	CREATE  = Action("create")
	DELETE  = Action("delete")
	COPY    = Action("copy")
	RENAME  = Action("rename")
	EXECUTE = Action("execute")
)

/* Set */
type Set struct {
	r  bool /* read */
	w  bool /* write */
	c  bool /* create */
	d  bool /* delete */
	cp bool /* copy */
	rn bool /* rename */
	x  bool /* execute */
}

/* Read */
func (s Set) Read() bool { return s.r }

/* Write */
func (s Set) Write() bool { return s.w }

/* Create */
func (s Set) Create() bool { return s.c }

/* Delete */
func (s Set) Delete() bool { return s.d }

/* Copy */
func (s Set) Copy() bool { return s.cp }

/* Rename */
func (s Set) Rename() bool { return s.rn }

/* Execute */
func (s Set) Execute() bool { return s.x }

func (s Set) String() string {
	tag := func(in string, b bool) string {
		if b {
			return fmt.Sprintf("%s:y", in)
		}
		return fmt.Sprintf("%s:n", in)
	}

	out := ""
	out += tag("r", s.r)
	out += "," + tag("w", s.w)
	out += "," + tag("c", s.c)
	out += "," + tag("d", s.d)
	out += "," + tag("cp", s.cp)
	out += "," + tag("rn", s.rn)
	out += "," + tag("x", s.x)

	return out
}

type v string

func (v v) String() string {
	return string(v)
}

type b bool

func (v b) String() string {
	if bool(v) {
		return "yes"
	}
	return "no"
}

type l string

func (v l) String() string {
	return fmt.Sprintf("%q", string(v))
}

func (s Set) Sexpr() *cell.Cell {
	return cell.List(v("abac-set"), cell.Cons(v("read"), b(s.r)),
		cell.Cons(v("write"), b(s.w)),
		cell.Cons(v("create"), b(s.c)),
		cell.Cons(v("delete"), b(s.d)),
		cell.Cons(v("copy"), b(s.cp)),
		cell.Cons(v("rename"), b(s.rn)),
		cell.Cons(v("execute"), b(s.x)))
}

/* Decode */
func (s Set) Decode(root *cell.Cell) Set {

	r := s.r
	w := s.w
	c := s.c
	d := s.d
	cp := s.cp
	rn := s.rn
	x := s.x

	if root == nil || !root.IsList() {
		return Set{r, w, c, d, cp, rn, x}
	}

	root = root.List()

	head := cell.First(root)

	//fmt.Printf("head = %q\n", head.SValue())

	if head.SValue() != "abac-set" {
		return Set{r, w, c, d, cp, rn, x}
	}

	rest := cell.Rest(root)

	for {
		if rest == nil {
			break
		}

		if rest.IsList() {
			list := rest.List()

			//fmt.Printf("%q\n", cell.Compact(list))

			/* should be a count of two for this list */
			if cell.Count(list) == 2 {

				head := cell.First(list)
				value := cell.Rest(list)

				//fmt.Printf("\thead=%q\n\ttail=%q\n", cell.Compact(head), cell.Compact(value))

				if value != nil && !value.IsList() {
					switch strings.ToLower(head.SValue()) {
					case "read":
						if value.SValue() == "yes" {
							r = true
						} else {
							r = false
						}
						break
					case "write":
						if value.SValue() == "yes" {
							w = true
						} else {
							w = false
						}
						break
					case "create":
						if value.SValue() == "yes" {
							c = true
						} else {
							c = false
						}
						break
					case "delete":
						if value.SValue() == "yes" {
							d = true
						} else {
							d = false
						}
						break
					case "copy":
						if value.SValue() == "yes" {
							cp = true
						} else {
							cp = false
						}
						break
					case "rename":
						if value.SValue() == "yes" {
							rn = true
						} else {
							rn = false
						}
						break
					case "execute":
						if value.SValue() == "yes" {
							x = true
						} else {
							x = false
						}
						break
					}
				}
			}
		}

		rest = rest.Next()
	}

	return Set{r, w, c, d, cp, rn, x}
}

/* Encode */
func (s Set) Encode() (string, error) {

	/* (abac-set (read yes) (write no)) */
	return sexpr.OutputString(s.Sexpr())
}

/* FalseSet */
var FalseSet = Set{r: false, w: false, c: false, d: false, cp: false, rn: false, x: false}

/* Object */
type Object interface {
	GetAttribute(key string, def []string) []string
}

/* Subject */
type Subject interface {
	GetAttribute(key string, def []string) []string
}

/* Environment */
type Environment struct {
	Time time.Time
}

/* Policy */
type Policy []Rule

/* Grant */
func (p Policy) Grant(builder *GrantBuilder) error {
	if builder == nil {
		return ErrInvalidGrantBuilder
	}
	if err := builder.Validate(); err != nil {
		return err
	}

	if builder.audit != "" {
		return ErrGrantBuilderAlreadyRun
	}

	s := FalseSet

	/* TODO: this needs a ruleset of how to add action-sets togeather, probably via the environment */
	for _, rule := range p {
		rs := rule.Eval(builder.subject, builder.object, builder.environ)
		s.r = rs.r
		s.w = rs.w
		s.c = rs.c
		s.d = rs.d
		s.cp = rs.cp
		s.rn = rs.rn
		s.x = rs.x
	}

	ans := false

	switch builder.action {
	case READ:
		ans = s.r
		break
	case WRITE:
		ans = s.w
		break
	case CREATE:
		ans = s.c
		break
	case DELETE:
		ans = s.d
		break
	case COPY:
		ans = s.cp
		break
	case RENAME:
		ans = s.rn
		break
	case EXECUTE:
		ans = s.x
		break
	}

	builder.answer = ans
	tag := "no"
	if ans {
		tag = "yes"
	}

	/* TODO: construct the entire audit string, including subject, object */

	audit := "done " + tag

	sha := sha1.New()
	sha.Write([]byte(audit))
	builder.audit = audit + " " + hex.EncodeToString(sha.Sum(nil))[:8]

	return nil
}

func (p Policy) Sexpr() *cell.Cell {

	rules := cell.New(nil, nil)

	for _, rule := range p {

		rules = cell.Append(rules, rule.Sexpr())
	}

	return cell.New(cell.List(v("abac-policy"), rules), nil)
}

/* Encode */
func (p Policy) Encode() (string, error) {

	return sexpr.OutputString(p.Sexpr())
}

/* GrantBuilder */
type GrantBuilder struct {
	subject Subject
	object  Object
	environ Environment
	action  Action

	answer bool
	audit  string
}

/* WithSubject */
func (builder *GrantBuilder) WithSubject(sub Subject) *GrantBuilder {
	builder.subject = sub
	return builder
}

/* WithObject */
func (builder *GrantBuilder) WithObject(object Object) *GrantBuilder {
	builder.object = object
	return builder
}

/* WithAction */
func (builder *GrantBuilder) WithAction(action Action) *GrantBuilder {
	builder.action = action
	return builder
}

/* WithEnvironment */
func (builder *GrantBuilder) WithEnvironment() *GrantBuilder {
	builder.environ = Environment{Time: time.Now()}
	return builder
}

/* Validate */
func (builder *GrantBuilder) Validate() error {
	if builder.subject == nil {
		return ErrInvalidGrantBuilder
	}
	if builder.object == nil {
		return ErrInvalidGrantBuilder
	}
	if builder.environ.Time.IsZero() {
		return ErrInvalidGrantBuilder
	}
	if string(builder.action) == "" {
		return ErrInvalidGrantBuilder
	}

	return nil
}

/* HaveGrant */
func (builder *GrantBuilder) HaveGrant() bool {
	return (builder.answer == true)
}

/* Audit */
func (builder *GrantBuilder) Audit() string {
	return builder.audit
}
