package backoff

import (
	"time"
	"errors"
	"crypto/rand"
)

/* TODO: add a context'd version so that cancel can be used 
 * TODO: add a box version so that time can be determined before use
 */

var (
	ErrBackoff = errors.New("Backoff")
	ErrTimeout = errors.New("Timeout")

	UseTestTimes = false
)

func sum(all ...time.Duration) time.Duration {

	u := time.Duration(0)

	for _,t := range all {
		u += t
	}

	return u
}


func double(d, max time.Duration) []time.Duration {

	out := make([]time.Duration,0)
	t := d

	for {
		if t >= max {
			break
		}
		
		out = append(out,t)
		
		t += d
	}

	out = append(out,max)

	return out
}

func noise(d time.Duration) time.Duration {

	b := make([]byte,2)
	rand.Read(b)

	t := uint64(b[1])	

	if b[0] > 128 {
		return d + time.Duration(t) * time.Millisecond
	}

	return d - time.Duration(t) * time.Millisecond
}


func defaultIntervals() []time.Duration {

	if UseTestTimes {
		return double(500 * time.Millisecond,1 * time.Second)
	}

	return double(5 * time.Second,1 * time.Minute)
}

/* Default */
func Default(f func() error) error {

	if f == nil {
		return nil
	}

	for _,d := range defaultIntervals() {
		
		if err := f(); err != nil {
			if err != ErrBackoff {
				return err
			}
		} else {
			return nil
		}

		time.Sleep(noise(d)) /* add a noise function here */
	}

	return ErrTimeout
}

func Backoff() error { return ErrBackoff }
