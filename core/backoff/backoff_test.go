package backoff

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
	"time"
	"fmt"
)


func Test_Backoff(t *testing.T) {
	Convey("Backoff",t,func() {
		
		classic := func() error {
				
			return Backoff()
		}

		Convey("internal",func() {
			Convey("double",func() {
				Convey("production",func() {
					l := double(5 * time.Second,1 * time.Minute) 	
					So(l,ShouldNotBeEmpty)
					So(len(l),ShouldEqual,12)
					fmt.Printf("%+v total of %s\n",l,sum(l...))
				})
				Convey("test",func() {
					l := double(500 * time.Millisecond,1 * time.Second)
					So(l,ShouldNotBeEmpty)
					So(len(l),ShouldEqual,2)
					fmt.Printf("%+v total of %s\n",l,sum(l...))
				})
			})
			Convey("noise",func() {
				So(noise(1 * time.Second),ShouldNotEqual,1 * time.Second)
			})
		})


		Convey("defaultIntervals times",func() {
			UseTestTimes = false

			d := defaultIntervals()
			So(d,ShouldNotBeEmpty)
			
		})



		Convey("Default",func() {
			UseTestTimes = true
			
			So(Default(classic),ShouldEqual,ErrTimeout)
		})
	})
}
