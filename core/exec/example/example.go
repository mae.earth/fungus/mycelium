package main

import (
	"os"
	"os/signal"
	"fmt"
	"time"
	"bufio"

	"mae.earth/fungus/mycelium/core/exec"
)

/* this is an example reference program running the mycelium exec standard */

func main() {

	/* TODO: parse args */


	sig := make(chan os.Signal)
	signal.Notify(sig,os.Interrupt)
	signal.Notify(sig,os.Kill)

	go loop(/* TODO: with args */)

	for {
		select {
			case <- sig:

			return
		}
	}
}

const (
	Command int = 0  /* setup the task and data attributes */
	Data int = 1     /* provide the data */
	Process int = 2  /* running task, will output a process format (0,100] */
)














type Attribute struct {
	Attribute string `exec:"name"`
	Name string `exec:"var"`
	Parameters bool `exec:"params"`
}

type DataBegin struct {
	DataBegin string `exec:"name"`
	Size int `exec:"var"`
}

type DataEnd struct {
	DataEnd string `exec:"name"`
}

func parse(in []byte) error {

	//fmt.Fprintf(os.Stdout,"[C]:Parsing %q\n",string(in))

	/* command stream is newline terminated space delimited 
   * [command] <arguments> [parameters] 
   * example: 
   * Attribute "object" "string name" ["foo"]
   * where command  is Attribute, argument is object and parameterlist is formed of paired key:value "string name" and ["foo"] 
   */

	info,err := exec.Parse(in,Attribute{},DataBegin{},DataEnd{})
	if err != nil {
		return err
	}


	fmt.Fprintf(os.Stdout,"[C]:parsed %q with %d vars and %d parameters\n",info.Command,len(info.Variables),len(info.Parameters))
	for i,param := range info.Parameters {
		fmt.Fprintf(os.Stdout,"\t%03d\t%30s (%10s) : %v\n",i + 1,param.Name,param.Type,param.Value)	
	}

	return nil
}




func loop(/* TODO: args */) {

	fmt.Fprintf(os.Stdout,"looping...\n")

	reader := bufio.NewReader(os.Stdin)

	mode := Command
	
	buf := make([]byte,512)
	pch := make(chan int,0)

	/* we must loop until told otherwise */
	for {
		if mode == Command {

			/* read in from standard input */
			text,err := reader.ReadString('\n')
			if err != nil {
				fmt.Fprintf(os.Stderr,"read error (ctrl mode) -- %v\n",err)
				continue
			}

			/* process the command and look for mode changes */
			if err := parse([]byte(text[:len(text) - 1])); err != nil {
				fmt.Fprintf(os.Stderr,"read error (ctrl mode) -- %v\n",err)
				continue
			}

			/* process depending on mode either {control,data} */
		//	fmt.Fprintf(os.Stdout,"[C] read %q\n",text)

			fmt.Fprintf(os.Stdout,"[C] switching mode\n")
			mode = Data

			continue
		}

		if mode == Process {

			/* then check for quit command */
			/* TODO */
			p := <- pch
			fmt.Fprintf(os.Stdout,"\r%03d",p)
			if p >= 100 {
				fmt.Fprintf(os.Stdout,"\r100\n")
				mode = Command
			}

			continue
		}
		
		/* in Data mode */
		n,err := reader.Read(buf)
		if err != nil {
			fmt.Fprintf(os.Stderr,"read error (data mode) -- %v",err)
			continue
		}

		fmt.Fprintf(os.Stdout,"[D] read %d bytes\n",n)

		/* scan for end of data to switch back into command mode */
		


		/* write out on standard output, any errors go to standard err */
		mode = Process
		pch = make(chan int,0)

		go process(pch)
		

	}
}

func process(pch chan int) {

	for i := 0; i < 100; i++ {

		pch <- i
		time.Sleep(100 * time.Millisecond)
	}

	pch <- 100

}



