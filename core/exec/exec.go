package exec

import (
	"fmt"
	"errors"
	"strings"
	"reflect"
	"strconv"
)

var (
	ErrNoTokens = errors.New("No Tokens")
	ErrNoCommands = errors.New("No Commands")
)



type Token struct {
	Raw string
	Type string
	LType string
	PType string
	N int /* used for array types */
}

/* Tokenise */
func Tokenise(in []byte) ([]*Token,error) {

	tokens := make([]*Token,0)
	word := ""

	for i := 0; i < len(in); i++ {
		c := rune(in[i])
				
		switch c {
			case ' ':
				if len(word) > 0 {
					tokens = append(tokens,&Token{Raw:word,Type:"string"})
					word = ""
				} 
				tokens = append(tokens,&Token{Raw:" ",Type:"space"})
			break
			case '"':
				if len(word) > 0 {
					tokens = append(tokens,&Token{Raw:word,Type:"string"})
					word = ""
				}
				tokens = append(tokens,&Token{Raw:"\"",Type:"literal"})
			break
			case '[':
				if len(word) > 0 {
					tokens = append(tokens,&Token{Raw:word,Type:"string"})
					word = ""
				}
				tokens = append(tokens,&Token{Raw:"[",Type:"open"})
			break
			case ']':
				if len(word) > 0 {
					tokens = append(tokens,&Token{Raw:word,Type:"string"})
					word = ""
				}
				tokens = append(tokens,&Token{Raw:"]",Type:"close"})
			break
			default:
				word += string(c)
			break
		}
	}
	if len(word) > 0 {
		tokens = append(tokens,&Token{Raw:word,Type:"string"})
	}

	return tokens,nil
}

/* Lexer */
func Lexer(in []*Token) ([]*Token,error) {
	if len(in) == 0 {
		return nil,ErrNoTokens
	}

	tokens := make([]*Token,len(in))

	literal := false
	brackets := false
	
	for i, token := range in {

		switch token.Type {
			case "literal":
				if !literal {
					token.LType = "begin"
				} else {
					token.LType = "end"
				}	
				literal = !literal
			
			break
			case "open":
				if !literal {
					if brackets {
						return nil,fmt.Errorf("error at token %d %q -- expecting to close bracket\n", i + 1, token.Raw)
					} 
					brackets = true
					token.LType = "begin"
				}
			break
			case "close":
				if !literal {
					if !brackets {
						return nil,fmt.Errorf("error at token %d %q -- need to open brackets first\n", i + 1, token.Raw)
					}
					brackets = false
					token.LType = "end"
				}
			break
			case "string":
				
				/* attempt to work out what this is */
				if !literal {
					
					switch token.Raw[0] {
						case '0','1','2','3','4','5','6','7','8','9':

							token.LType = "number"
						break
						default:
	
							token.LType = "alphanum"
						break
					}


				} else {
					token.LType = "string"
				}

			break
			default:
				token.LType = "--"
			break
		}	

		tokens[i] = token
	}


	return tokens,nil
}


/* Parse tokens and compare against control structs */
func Parser(in []*Token /* TODO: include ctx */) ([]*Token,error) {

	parse := func(tkn string) (string,string,error) {

		parts := strings.Split(tkn," ")
		if len(parts) != 2 {
			return "--","--",fmt.Errorf("expected two parts but was %d instead",len(parts))
		}

		switch parts[0] {
			case "int", "float", "string":
		
			break
			default:
			
				return "--","--",fmt.Errorf("unknown type %q",parts[0])
			break
		}

		return parts[0],parts[1],nil
	}



	tokens := make([]*Token,0)

	begun := false
	current := ""
	dbegun := false
	dcount := 0

	var lasttoken  *Token

	for _,token := range in {
	
		/* look for literal begins and ends at compress into a token */
		if token.Type == "literal" && token.LType == "begin" && !begun {
			begun = true
			current = ""
			continue
		}

		if token.Type == "literal" && token.LType == "end" && begun {
			begun = false
			if len(current) > 0 && !dbegun {
				tokens = append(tokens,&Token{Raw:current, Type: "string", LType: "token"})
				lasttoken = tokens[len(tokens) - 1]
			}
			continue
		}

		if token.LType == "alphanum" && token.Raw == "#" {
			/* then comment we can stop here */
			break
		}

		if token.Type == "open" && token.LType == "begin" && !dbegun {
			dbegun = true
			dcount = 0
			continue
		}

		if token.Type == "close" && token.LType == "end" && dbegun {
			dbegun = false

			/* go back and apply array to type if not set */
			if lasttoken != nil {
				itype,_,err := parse(lasttoken.Raw)
				if err != nil {
					return nil,err
				}
		
				lasttoken.PType = itype
				lasttoken.N = dcount
		
			} else {
				return nil,fmt.Errorf("no last token")
			}

			continue
		}

		
		if begun || dbegun {
			if begun {
				current += token.Raw
			}
			
			if dbegun {
				if lasttoken == nil {
					return nil,fmt.Errorf("error -- was expecting a token with type information")
				}

				if token.Type == "space" {

					continue
				}	

				_,iname,err := parse(lasttoken.Raw)
				if err != nil {
					return nil,err
				}
				

				token.PType = fmt.Sprintf("%s[%d]",iname,dcount)
				tokens = append(tokens,token)
				dcount ++
			}
			continue
		}
		
		if token.Type != "space" {

			tokens = append(tokens,token)
		}	
	}


	return tokens,nil
}

type ExampleCommand struct {
	Attribute string `exec:"name"`
	Name string `exec:"var"`	
	Number int `exec:"var"`
	Float float64 `exec:"var"`
	Parameters bool `exec:"params"`
}

type Truple struct {
	Name string
	Type string
	Value interface{}
}


type Information struct {
	Command string
	Variables []Truple
	Parameters []Truple
}
	
func Map(in []*Token, structs ...interface{}) (*Information,error) {

	if len(structs) == 0 {
		return nil,ErrNoCommands
	}

	for i,s := range structs {

		st := reflect.TypeOf(s)
		if st.Kind() != reflect.Struct {
			return nil,fmt.Errorf("expecting struct not a %q at %d input",st.Kind().String(),i)
		}
	}


	info := &Information{Command:"nop",Variables:nil,Parameters:nil}

	var current reflect.Type
	pos := 0
	parampos := 0

	for i,token := range in {
		if i == 0 {
			if token.LType != "alphanum" {
				return nil,fmt.Errorf("expecting alphanum but was %q\n",token.LType)
			}
			info.Command = token.Raw

			/* check through the structs and find the command */
			found := false

			for _,s := range structs {
				st := reflect.TypeOf(s)
				for j := 0; j < st.NumField(); j++ {
					field := st.Field(j)
					if field.Tag.Get("exec") == "name" {
						if field.Name == token.Raw {							
							current = st
							found = true
						}
					}
					if found {
						break
					}
				}
			}
			if !found {
				return nil,fmt.Errorf("error looking up command %q",token.Raw)
			}
						

			info.Variables = make([]Truple,0)

			/* go through all get all the var names/types */
			for j := 0; j < current.NumField(); j++ {
				field := current.Field(j)
				switch field.Tag.Get("exec") {
					case "var":
						info.Variables = append(info.Variables,Truple{Name: field.Name, Type: field.Type.String(), Value: nil})
					break
					case "params":
						info.Parameters = make([]Truple,0)
					break
				}			
			}

			continue
		}

		/* mapping struct values */
		if info.Variables != nil {
			if pos < len(info.Variables) {
			
				fmt.Printf("pos=%d, token=%+v\n",pos,token)

				switch token.LType {
					case "token","string":
				
						if info.Variables[pos].Type != "string" {
							return nil,fmt.Errorf("expecting type %q for variable %d", info.Variables[pos].Type, pos)
						}
						
						info.Variables[pos].Value = token.Raw
						pos++
					break
					case "number":

						switch info.Variables[pos].Type {
							case "int":

								d,err := strconv.ParseInt(token.Raw,10,64)
								if err != nil {
									return nil,err
								}
								info.Variables[pos].Value = d	

							break
							case "float64":

								f,err := strconv.ParseFloat(token.Raw,64)
								if err != nil {
									return nil,err
								}
								info.Variables[pos].Value = f
	

							break
							default:

								return nil,fmt.Errorf("error unknown value type %q", info.Variables[pos].Type)
							break
						}

						pos++
					break
				}

				continue
			}
		}

		if info.Parameters == nil {
			return nil,fmt.Errorf("not expecting parameterlist")
		}


		/* parameter list */
		if parampos == 0 { /* then should be token */
			if token.LType != "token" {
				return nil,fmt.Errorf("expecting token for parameter information")
			}
			
			info.Parameters = append(info.Parameters,Truple{Name: token.Raw, Type: token.PType})	
			parampos = token.N
		} else { /* then value */

			param := &info.Parameters[len(info.Parameters) - 1]

			switch param.Type {
				case "string":
					if param.Value == nil {
						if parampos == 1 {
							param.Value = token.Raw
						} else {
							param.Value = []string{token.Raw}
						}
					} else {
						if v,ok := param.Value.([]string); ok {
							v = append(v,token.Raw)
							param.Value = v
						} else {
							return nil,fmt.Errorf("error adding to string array")
						}
					}
				break
				case "float":
					f,err := strconv.ParseFloat(token.Raw,64)
					if err != nil {
						return nil,err
					}

					if param.Value == nil {
						if parampos == 1 {
							param.Value = f
						} else {
							param.Value = []float64{f}
						}
					} else {
						if v,ok := param.Value.([]float64); ok {
							v = append(v,f)
							param.Value = v
						} else {
							return nil,fmt.Errorf("error adding to float array")
						}
					}							

				break
				case "int":

					d64,err := strconv.ParseInt(token.Raw,10,64)
					if err != nil {
						return nil,err
					}
					d := int(d64)

					if param.Value == nil {
						if parampos == 1 {
							param.Value = d
						} else {
							param.Value = []int{d}
						}
					} else {
						if v,ok := param.Value.([]int); ok {
							v = append(v,d)
							param.Value = v
						} else {
							return nil,fmt.Errorf("error adding to int array")
						}
					}							

				break
			}
		
			parampos --
		}

	}


	


	return info,nil
}


func Parse(in []byte,structs ...interface{}) (*Information,error) {

	tokens,err := Tokenise(in)
	if err != nil {
		return nil,err
	}

	tokens,err = Lexer(tokens)
	if err != nil {
		return nil,err
	}

	tokens,err = Parser(tokens)
	if err != nil {
		return nil,err
	}

	return Map(tokens,structs...)
}





