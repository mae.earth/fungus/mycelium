package exec

import (
	"fmt"
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func print(i interface{}) {

	if tokens,ok := i.([]*Token); ok {
		recon := ""

		for i,token := range tokens {
			if token.Type == "string" {
				if token.LType == "token" {
					recon += " "
				}
				recon += token.Raw
				fmt.Printf("[%05d]\t%6s\t%8s\t%10s/%d | %s\n",i + 1,token.Type,token.LType,token.PType,token.N,token.Raw)
			} else {
				if token.LType == "token" {
					recon += " "
				}
				recon += token.Raw
				fmt.Printf("[%05d]\t%6s\t%8s\t%10s/%d\n",i + 1,token.Type,token.LType,token.PType,token.N)
			}
		}
		fmt.Printf("\n%d tokens\n%s\n",len(tokens),recon)
		return
	}

	if info,ok := i.(*Information); ok {
		fmt.Printf("[%s]\n",info.Command)
		for i,vari := range info.Variables {
			fmt.Printf("\tvar   %03d: %30s (%s) = %v\n",i + 1, vari.Name, vari.Type, vari.Value)
		}
		for i,param := range info.Parameters {
			fmt.Printf("\tparam %03d: %30s (%s) = %v\n",i + 1,param.Name,param.Type,param.Value) 
		}	
	}
}

func Test_ExecParser(t *testing.T) {
	Convey("ExecParser",t,func() {

			Convey("Simple",func() {
				tokens,err := Tokenise([]byte(`Attribute "one" 5 0.1234 "string name" ["first"] "int count" [101] "int n" [5 6 4 3] "string name" ["second"] # "string test" ["foo" "bar" "soo"]`))
				So(err,ShouldBeNil)
				So(tokens,ShouldNotBeEmpty)
				//So(len(tokens),ShouldEqual,17)

				print(tokens)

				Convey("Lexer",func() {
					tokens,err = Lexer(tokens)
					So(err,ShouldBeNil)
					So(tokens,ShouldNotBeEmpty)

					print(tokens)

					Convey("Parser",func() {
						tokens,err = Parser(tokens)
						So(err,ShouldBeNil)

						print(tokens)

						Convey("Map",func() {
							info,err := Map(tokens,ExampleCommand{})
							So(err,ShouldBeNil)
		
							print(info)
						})
					})
				})
			})



	})
}


