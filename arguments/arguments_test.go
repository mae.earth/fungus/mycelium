/* mae.earth/fungus/mycelium/arguments/arguments_test.go
 * mae 012018 LYHE
 */
package arguments

import (
	"strings"
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func f(str ...string) string {
	return strings.Join(str,":")
}

func Test_ParseArguments(t *testing.T) {
	Convey("Parse Arguments",t,func() {
		Convey("empty",func() {

			v,err := Parse([]string{},0)			
			So(err,ShouldEqual,ErrEmpty)
			So(v,ShouldBeEmpty)
		})

		Convey("bad skip",func() {

			v,err := Parse([]string{"-tags","a","foo"},10)
			So(err,ShouldEqual,ErrEmpty)
			So(v,ShouldBeEmpty)
		})

		Convey("bad key",func() {
	
			v,err := Parse([]string{"-tags","a","foo","bar","-alias","Alice"},1)
			So(err,ShouldEqual,ErrBadKey)
			So(v,ShouldBeEmpty)
		})

		Convey("normal",func() {

			v,err := Parse([]string{"-tags","a","foo","bar","-alias","Alice","-weight","10","-foo","0.56"},0)
			So(err,ShouldBeNil)
			So(v,ShouldNotBeNil)
			So(v.String(),ShouldEqual,`-alias Alice -foo 0.56 -tags a foo bar -weight 10`)

			Convey("-tags",func() {

				values := v["tags"]
				So(values,ShouldNotBeEmpty)
				So(f(values...),ShouldEqual,f("a","foo","bar"))
				So(v.First("tags"),ShouldEqual,"a")
				So(v.Last("tags"),ShouldEqual,"bar")
			})

			Convey("-alias",func() {

				So(v["alias"],ShouldNotBeEmpty)
				So(f(v["alias"]...),ShouldEqual,f("Alice"))
			})

			Convey("-weight is an integer",func() {
				
				So(v.FirstInt("weight",-10),ShouldEqual,10)
				So(v.FirstInt("null",-10),ShouldEqual,-10)
			})

			Convey("-foo is an float",func() {

				So(v.FirstFloat("foo",-1.0),ShouldEqual,0.56)
				So(v.FirstFloat("null",-1.0),ShouldEqual,-1.0)
			})
		})

	})
}
