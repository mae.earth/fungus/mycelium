/* mae.earth/fungus/mycelium/arguments/arguments.go
 * mae 012018 LYHE
 */
package arguments

import (
	"strings"
	"strconv"
	"errors"
	"sort"
)

var (
	ErrEmpty error = errors.New("Empty")
	ErrBadKey error = errors.New("Bad Key")
)

/* Values */
type Values map[string][]string

func (v Values) String() string {

	str := ""

	keys := []string{}
	for k,_ := range v {
		keys = append(keys,k)
	}

	sort.Slice(keys,func(i,j int) bool { return keys[i] < keys[j] })

	for _,k := range keys {
		values := v[k]
		if len(values) > 0 {
			if len(str) > 0 {
				str += " "
			}
			str += "-" + k + " " + strings.Join(values," ")
		}
	}

	return str
}
			

/* First */
func (v Values) First(key string) string {
	
	if values,ok := v[key]; ok {
		if len(values) > 0 {
			return values[0]
		}
	} 
	return ""
}

func (v Values) FirstInt(key string,def int) int {

	if values,ok := v[key]; ok {
		if len(values) > 0 {
			d,err := strconv.ParseInt(values[0],10,64)
			if err == nil {
				return int(d)
			}
		}
	}

	return def
}

func (v Values) FirstFloat(key string,def float64) float64 {

	if values,ok := v[key]; ok {
		if len(values) > 0 {
			d,err := strconv.ParseFloat(values[0],64)
			if err == nil {
				return d
			}
		}
	}
	return def
}

func (v Values) FirstString(key,def string) string {

	if values,ok := v[key]; ok {
		if len(values) > 0 {
			return values[0]
		}
	}
	return def
}

/* Last */
func (v Values) Last(key string) string {

	if values,ok := v[key]; ok {
		if len(values) > 0 {
			return values[len(values) - 1]
		}
	}
	return ""
}

/* Parse */
func Parse(args []string,skip int) (Values,error) {

	v := make(map[string][]string,0)

	if len(args) == 0 || len(args) - skip <= 0 {
		return v,ErrEmpty
	}

	key := ""
	values := []string{}

	for i := skip; i < len(args); i++ {
		
		if args[i][0] == '-' {
			if len(key) > 0 {
				v[key] = values
				values = []string{}
			}
			key = args[i][1:]
			continue
		}

		if len(key) == 0 {
			return v,ErrBadKey
		}

		values = append(values,args[i])
	}
	/* get tail */
	if len(key) > 0 && len(values) > 0 {
		v[key] = values
	}
	
	return v,nil
}

func New() Values {
	return make(map[string][]string,0)
}
