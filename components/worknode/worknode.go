/* mae.earth/fungus/mycelium/components/worknode/worknode.go
 * mae 012018 LYHE
 */
package worknode

import (
	"sync"
	"net/http"
	"github.com/gorilla/mux"

	"mae.earth/fungus/mycelium/components"
	"mae.earth/fungus/mycelium/network"
)

/* TODO: all the files (such as svg, css etc) are located someplace on the mycelium network, 
 * otherwise we use the built-in html template. 
 */

const (
	Name string = "worknode"
)

/* process TODO: add progress channel/info etc */
func process(env *components.Environment,quitch chan bool) {

	for {
		select {
			case <- quitch:
				return
			break
		}
	}
}


type Component struct {
	mux sync.RWMutex
	quitch chan bool

	env *components.Environment
}

/* Name */
func (c *Component) Name() string {
	return Name
}

/* Begin */
func (c *Component)	Begin(identity string, env *components.Environment) error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.quitch != nil {
		return components.ErrAlreadyBegun
	}

	c.quitch = make(chan bool,1)

	c.env = env.Copy()


	/* run go routine */
	go process(c.env,c.quitch)


	return nil
}
	
/* End */
func (c *Component)	End() error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.quitch == nil {
		return components.ErrNotBegun
	}

	close(c.quitch)


	return nil
}

/* Status */
func (c *Component) Status(nodes []*network.Node) int {
	c.mux.RLock()
	defer c.mux.RUnlock()

	if c.quitch == nil {
		return -1
	}

	return 0
}
	

/* Mount */
func (c *Component) Mount(root *mux.Router) error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.quitch == nil {
		return components.ErrNotBegun
	}

	if root == nil {
		return nil
	}

	/* TODO: version the api */

	root.HandleFunc("/", c.IndexHandler)
	root.HandleFunc("/status", c.StatusHandler)
	root.HandleFunc("/admin", c.AdminHandler)
	/* do more */

	return nil
}

/* Unmount */
func (c *Component) Unmount() error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.quitch == nil {
		return components.ErrNotBegun
	}

	return nil
}


/* New */
func New() *Component {

	c := &Component{}

	return c
}

	
/* TODO: wrap with middleware */
func (c *Component) IndexHandler(w http.ResponseWriter,req *http.Request) {
	c.mux.RLock()
	defer c.mux.RUnlock()

	/* TODO */

	w.Write([]byte("WORKNODE INDEX"))
}

func (c *Component) StatusHandler(w http.ResponseWriter,req *http.Request) {
	c.mux.RLock()
	defer c.mux.RUnlock()
	
	/* TODO */

	w.Write([]byte("0"))
}
	

func (c *Component) AdminHandler(w http.ResponseWriter,req *http.Request) {
	c.mux.RLock()
	defer c.mux.RUnlock()

	/* TODO */
	w.Write([]byte("admin"))
}







