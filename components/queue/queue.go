/* mae.earth/fungus/mycelium/components/queue/queue.go
 * mae 012018 LYHE
 */
package queue

import (
	"path"
	"sync"
	"time"
	"net/http"
	"github.com/gorilla/mux"
	bolt "github.com/coreos/bbolt"

	"mae.earth/fungus/mycelium/components"
	"mae.earth/fungus/mycelium/network"
)


const (
	Name string = "queue"
)

/* process TODO: add progress channel/info etc */
func process(env *components.Environment,quitch chan bool) {

	for {
		select {
			case <- quitch:
				return
			break
		}
	}
}


type Component struct {
	mux sync.RWMutex
	quitch chan bool

	db *bolt.DB
	env *components.Environment
}

/* Name */
func (c *Component) Name() string {
	return Name
}

/* Begin TODO: pass in identity and work path */
func (c *Component)	Begin(identity string, env *components.Environment) error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.quitch != nil {
		return components.ErrAlreadyBegun
	}

	c.quitch = make(chan bool,1)

	c.env = env.Copy()

	/* queue.db is located at env.Dir + /<identity>/queue.db */
	file := path.Join(env.Dir,identity,"queue.db")	

	db,err := bolt.Open(file,0660,&bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		close(c.quitch)
		return err	
	}

	c.db = db


	/* run go routine */
	go process(c.env,c.quitch)


	return nil
}
	
/* End */
func (c *Component)	End() error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.quitch == nil {
		return components.ErrNotBegun
	}

	close(c.quitch)

	if c.db == nil {
		return nil
	}

	return c.db.Close()
}

/* Status */
func (c *Component) Status(nodes []*network.Node) int {
	c.mux.RLock()
	defer c.mux.RUnlock()

	if c.quitch == nil {
		return -1
	}

	return 0
}
	

/* Mount */
func (c *Component) Mount(root *mux.Router) error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.quitch == nil {
		return components.ErrNotBegun
	}

	if root == nil {
		return nil
	}

	/* TODO: version the api */

	root.HandleFunc("/", c.IndexHandler)
	root.HandleFunc("/status", c.StatusHandler)
	root.HandleFunc("/admin", c.AdminHandler)

	/* queue is designed to have a very simple minimal setup 
   *
   * for each channel (queue) it can be setup with specific attributes and limits, 
   * including security, the user(s) can enqueue/dequeue/delete/duplicate items, 
   * where the item (or message) has a storage limit -- as set when the channel was
   * setup (within maximum hard limits) 
   * 
   */

	/* information about the message is set in the header of the request */

	root.HandleFunc("/c/{queue}/{message}", c.GetMessageHandler).Methods("GET")
	root.HandleFunc("/c/{queue}/{message}", c.PutMessageHandler).Methods("PUT")
	root.HandleFunc("/c/{queue}/{message}", c.DeleteMessageHandler).Methods("DELETE")

	root.HandleFunc("/c/{queue}", c.GetQueueHandler).Methods("GET")
	root.HandleFunc("/c/{queue}", c.PutQueueHandler).Methods("PUT")
	root.HandleFunc("/c/{queue}", c.PostQueueHandler).Methods("POST")
	root.HandleFunc("/c/{queue}", c.DeleteQueueHandler).Methods("DELETE")

	root.HandleFunc("/c", c.QueueIndexHandler).Methods("GET")
	root.HandleFunc("/c", c.PostQueueHandler).Methods("POST") 

	return nil
}

/* Unmount */
func (c *Component) Unmount() error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.quitch == nil {
		return components.ErrNotBegun
	}

	return nil
}


/* New */
func New() *Component {

	c := &Component{}

	return c
}

func (c *Component) StatusHandler(w http.ResponseWriter,req *http.Request) {
	c.mux.RLock()
	defer c.mux.RUnlock()
	
	/* TODO */

	w.Write([]byte("0"))
}
	
/* TODO: wrap with middleware */
func (c *Component) IndexHandler(w http.ResponseWriter,req *http.Request) {

	

	/* TODO */

	w.Write([]byte("QUEUE INDEX"))
}


	

func (c *Component) AdminHandler(w http.ResponseWriter,req *http.Request) {
	c.mux.RLock()
	defer c.mux.RUnlock()

	/* TODO */
	w.Write([]byte("admin"))
}


func (c *Component) GetMessageHandler(w http.ResponseWriter, req *http.Request) {
	c.mux.RLock()
	defer c.mux.RUnlock()

	w.Write([]byte("GET ok"))
}

/* PutMessageHandler */
func (c *Component) PutMessageHandler(w http.ResponseWriter, req *http.Request) {
	c.mux.Lock()
	defer c.mux.Unlock()

	w.Write([]byte("PUT"))
}

/* DeleteMessageHandler */
func (c *Component) DeleteMessageHandler(w http.ResponseWriter, req *http.Request) {
	
	err := c.db.Update(func(tx *bolt.Tx) error {

		/* TODO */

		return nil
	})

	if err != nil {
		http.Error(w,err.Error(),500)
		return
	}


	w.Write([]byte("DELETE"))
}

/* GetQueueHandler */
func (c *Component) GetQueueHandler(w http.ResponseWriter, req *http.Request) {
	
	err := c.db.View(func(tx *bolt.Tx) error {

		/* TODO */

		return nil
	})

	if err != nil {
		http.Error(w,err.Error(),500)
		return
	}


	w.Write([]byte("GET QUEUE"))
}

/* PutQueueHandler */
func (c *Component) PutQueueHandler(w http.ResponseWriter, req *http.Request) {
	
	err := c.db.Update(func(tx *bolt.Tx) error {

		/* TODO */

		return nil
	})

	if err != nil {
		http.Error(w,err.Error(),500)
		return 
	}

	w.Write([]byte("PUT QUEUE"))
}

/* PostQueueHandler */
func (c *Component) PostQueueHandler(w http.ResponseWriter, req *http.Request) {

	/* TODO: add a message to queue */
	
	err := c.db.Update(func(tx *bolt.Tx) error {

		/* TODO */

		return nil
	})

	if err != nil {
		http.Error(w,err.Error(),500)
		return
	}

	w.Write([]byte("POST QUEUE"))
}

/* DeleteQueueHandler */
func (c *Component) DeleteQueueHandler(w http.ResponseWriter, req *http.Request) {
	
	err := c.db.Update(func(tx *bolt.Tx) error {
	
		/* TODO */
	
		return nil
	})

	if err != nil {
		http.Error(w,err.Error(),500)
		return
	}

	w.Write([]byte("DELETE QUEUE"))
}

/* QueueIndexHandler */
func (c *Component) QueueIndexHandler(w http.ResponseWriter, req *http.Request) {

	/* TODO: read in the filters, settings from the request header */


	err := c.db.View(func(tx *bolt.Tx) error {

		/* TODO : read all the queues and general settings then present to the user */

		return nil
	})

	if err != nil {
		http.Error(w,err.Error(),500)
		return
	}	

	w.Write([]byte("QUEUE INDEX"))
}

func (c *Component) PostQueueIndexHandler(w http.ResponseWriter, req *http.Request) {
	
	/* TODO: decide what the user wants here : usually add a queue */

	err := c.db.Update(func(tx *bolt.Tx) error {

		/* TODO: add a queue */
		return nil
	})

	if err != nil {
		http.Error(w,err.Error(),500)
		return
	}

	w.Write([]byte("POST QUEUE"))
}


/* TODO: use sleeve */
type Queue struct {



}











