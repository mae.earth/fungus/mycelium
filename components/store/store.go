/* mae.earth/fungus/mycelium/components/store/store.go
 * mae 012018 LYHE
 */
package store

import (
	"sync"
	"net/http"
	"github.com/gorilla/mux"

	"mae.earth/fungus/mycelium/components"
	"mae.earth/fungus/mycelium/network"
)


const (
	Name string = "store"
)

/* process TODO: add progress channel/info etc */
func process(env *components.Environment,quitch chan bool) {

	for {
		select {
			case <- quitch:
				return
			break
		}
	}
}

/* Component */
type Component struct {
	mux sync.RWMutex
	quitch chan bool

	env *components.Environment


	/* TODO: number of workspaces as backstored with ll/store/limit; 
   * they should come and go over time
	 */ 


}

/* Name */
func (c *Component) Name() string {
	return Name
}

/* Begin */
func (c *Component)	Begin(identity string, env *components.Environment) error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.quitch != nil {
		return components.ErrAlreadyBegun
	}

	c.quitch = make(chan bool,1)

	c.env = env.Copy()


	/* run go routine */
	go process(c.env,c.quitch)


	return nil
}
	
/* End */
func (c *Component)	End() error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.quitch == nil {
		return components.ErrNotBegun
	}

	close(c.quitch)


	return nil
}

/* Status */
func (c *Component) Status(nodes []*network.Node) int {
	c.mux.RLock()
	defer c.mux.RUnlock()

	if c.quitch == nil {
		return -1
	}

	return 0
}
	

/* Mount */
func (c *Component) Mount(root *mux.Router) error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.quitch == nil {
		return components.ErrNotBegun
	}

	if root == nil {
		return nil
	}

	/* TODO: version the api */

	root.HandleFunc("/", c.IndexHandler)
	root.HandleFunc("/status", c.StatusHandler)
	root.HandleFunc("/admin", c.AdminHandler)

	/* store has a number of file based REST system :-
	 *
   * PUT, POST (process), DELETE, GET, HEAD
   *
   */

	/* store uses ll/limit for storage, this should allow fast replication between
   * storage nodes 
   */


	
	return nil
}

/* Unmount */
func (c *Component) Unmount() error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.quitch == nil {
		return components.ErrNotBegun
	}

	return nil
}


/* New */
func New() *Component {

	c := &Component{}

	return c
}

	
/* IndexHandler -- TODO: wrap with middleware */
func (c *Component) IndexHandler(w http.ResponseWriter,req *http.Request) {
	c.mux.RLock()
	defer c.mux.RUnlock()

	/* TODO */

	w.Write([]byte("STORE INDEX"))
}

/* StatusHandler */
func (c *Component) StatusHandler(w http.ResponseWriter,req *http.Request) {
	c.mux.RLock()
	defer c.mux.RUnlock()
	
	/* TODO */

	w.Write([]byte("0"))
}
	
/* AdminHandler */
func (c *Component) AdminHandler(w http.ResponseWriter,req *http.Request) {
	c.mux.RLock()
	defer c.mux.RUnlock()

	/* TODO */
	w.Write([]byte("admin"))
}







