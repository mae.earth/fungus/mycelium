/* mae.earth/fungus/mycelium/components/definitions.go
 * mae 012018 LYHE
 */
package components

import (
	"fmt"
	"time"
	"sort"
	"strings"
	"errors"
	"github.com/gorilla/mux"

	"mae.earth/fungus/mycelium/network"
	"mae.earth/fungus/mycelium/arguments"
)

var (
	ErrNotBegun error = errors.New("Not Begun")
	ErrAlreadyBegun error = errors.New("Already Begun")
)

/* dedup -- TODO: move to utilities */
func dedup(s []string, strs ...string) []string {

	p := make(map[string]bool, 0)

	for _, k := range s {
		p[k] = true
	}

	for _, k := range strs {
		p[k] = true
	}

	out := make([]string, len(p))
	i := 0

	for k := range p {
		out[i] = k
		i++
	}

	sort.Slice(out, func(i, j int) bool { return out[i] < out[j] })

	return out
}





/* TODO: require an interface for registering what the component is interested in from the mycelium network. 
 * an example would be that the worknode component will need to know about blackboards, bootstraps etc..
 */

/* Component */
type Component interface {

	Name() string

	/* Begin */
	Begin(identity string,env *Environment) error 
	
	/* End */
	End() error	

	/* status of service */
	Status([]*network.Node) int

	/* for mounting http interfaces */
	Mount(*mux.Router) error 
	Unmount() error

}

/* Environment */
type Environment struct {
	Port int /* http service port */

	Alias string
	Dir string
	See string
	
	Weight int

	Tags map[string]string
	Attributes map[string]string

	StartTime time.Time
}

/* String */
func (env *Environment) String() string {

	return fmt.Sprintf("alias=%s, dir=%s, see=%s, weight=%d with %d tags and %d attributes",env.Alias,env.Dir,env.See,env.Weight,len(env.Tags),len(env.Attributes))
}

/* PrettyPrint */
func (env *Environment) PrettyPrint() string {

	var b strings.Builder 

	fmt.Fprintf(&b,"alias\t%q\n", env.Alias)
	fmt.Fprintf(&b,"weight\t%d\n", env.Weight)
	fmt.Fprintf(&b,"dir\t%q\n", env.Dir)
	fmt.Fprintf(&b,"see\t%q\n", env.See)
	fmt.Fprintf(&b,"tags:\n")
	for k, v := range env.Tags {
		fmt.Fprintf(&b,"\t%s=%s\n", k, v)
	}
	fmt.Fprintf(&b,"attributes:\n")
	for k, v := range env.Attributes {
		fmt.Fprintf(&b,"\t%s=%s\n", k, v)
	} 

	return b.String()
}

/* Copy */
func (env *Environment) Copy() *Environment {

	e := &Environment{Port:env.Port,Alias:env.Alias,Dir:env.Dir,See:env.See,Weight:env.Weight,StartTime:env.StartTime}
	e.Tags = make(map[string]string,0)
	e.Attributes = make(map[string]string,0)
	

	for k,v := range env.Tags {
		e.Tags[k] = v
	}

	for k,v := range env.Attributes {
		e.Attributes[k] = v
	}

	return e
}

/* Export */
func (env *Environment) Export() []string {

	out := []string{fmt.Sprintf("alias=%s",env.Alias),fmt.Sprintf("dir=%s",env.Dir),fmt.Sprintf("see=%s",env.See),fmt.Sprintf("weight=%d",env.Weight)}
	
	for k,values := range env.Tags {
		out = append(out,fmt.Sprintf("T:%s=%s",k,values))
	}

	for k,values := range env.Attributes {
		out = append(out,fmt.Sprintf("A:%s=%s",k,values))
	}

	return out
}


/* NewEnvironment */
func NewEnvironment() *Environment {

	e := &Environment{StartTime:time.Now()}
	e.Tags = make(map[string]string,0)
	e.Attributes = make(map[string]string,0)

	return e
}



func NewEnvironmentFromArgs(args arguments.Values) *Environment {

	env := NewEnvironment()

	env.Alias = args.FirstString("alias","")
	env.Dir = args.FirstString("dir","")
	env.See = args.FirstString("see","")
	env.Weight = args.FirstInt("weight",0)
	
	if values,ok := args["tags"]; ok {
		for _,v := range values {
			
			if strings.Contains(v, "=") {

				parts := strings.Split(v, "=")
				if len(parts) != 2 {
					continue
				}

				bits := []string{parts[1]}

				if strings.Contains(parts[1], ";") {
					bits = strings.Split(parts[1], ";")
				}

				if c, ok := env.Tags[parts[0]]; !ok {
					env.Tags[parts[0]] = strings.Join(dedup(bits), ";")
				} else {
					env.Tags[parts[0]] = strings.Join(dedup(strings.Split(c, ";"), bits...), ";")
				}
			}			
		}
	}	

	if values,ok := args["attributes"]; ok {
		for _,v := range values {
			if strings.Contains(v, "=") {

				parts := strings.Split(v, "=")
				if len(parts) != 2 {
					continue
				}

				bits := []string{parts[1]}

				if strings.Contains(parts[1], ";") {
					bits = strings.Split(parts[1], ";")
				}

				if c, ok := env.Attributes[parts[0]]; !ok {
					env.Attributes[parts[0]] = strings.Join(dedup(bits), ";")
				} else {
					env.Attributes[parts[0]] = strings.Join(dedup(strings.Split(c, ";"), bits...), ";")
				}
			}
		}
	}

	return env
}

