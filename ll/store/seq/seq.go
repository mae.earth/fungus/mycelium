/* mae.earth/fungus/mycelium/ll/store/seq.go
 * mae 012018 LYHE
 *
 * sequental store, useful for queue implementation 
 */
package seq

import (
	"strings"
	"context"
	"errors"
	"time"
	"sync"
	"encoding/hex"
	"crypto/rand"
)

/* technical notes : 
 * 
 */

/* TODO: 
 * content should include Content-Type
 * all functions should require a set of sorting functions 
 * all channels should include attributes/configuration system
 * lookup via identifier
 * backstore in a bolt database
 * limited storage amount; actual content should be stored elsewhere
 */

/* Identity -- should be generated out of package */

var (
	ErrEmpty = errors.New("Empty")
	ErrBadContent = errors.New("Bad Content")
	ErrBadChannel = errors.New("Bad Channel")
	ErrBadInfo = errors.New("Bad Information")
	ErrFiltered = errors.New("Filtered")
)

/* TODO: all this is for development only */
type ChannelMetadata struct {
	Id string
	Appended time.Time
	Attributes map[string]string      /* FIXME: replace with read-only version that is set when appended, static-one-way version with hidden attributes and a Get/Has interface */
	Tags map[string]string

	ContentType string
	Version int
}

func (m *ChannelMetadata) Copy() *ChannelMetadata {
	meta := &ChannelMetadata{Id:m.Id,Appended:m.Appended,Attributes: make(map[string]string,0),
													 ContentType:m.ContentType,Tags:make(map[string]string,0),Version:m.Version}
	
	for key,value := range m.Attributes {
		meta.Attributes[key] = value
	}

	for key,value := range m.Tags {
		meta.Tags[key] = value
	}

	return meta
}

func NewMeta(id,ctype string) *ChannelMetadata {
	return &ChannelMetadata{Id:id, ContentType:ctype, 
													Appended: time.Now(), Attributes: make(map[string]string,0),Tags:make(map[string]string,0),Version:0}
}


type ChannelData struct {

	Content []byte

	Meta *ChannelMetadata	
}



type Channel struct {
	mux sync.RWMutex
	Data []*ChannelData
}


var internal struct {
	channels map[string]*Channel 

}

func init() {
	
	internal.channels = make(map[string]*Channel,0)
}

/* TODO: move outwards and standardise */
func identifier() string {
	b := make([]byte,8)
	rand.Read(b)
	return hex.EncodeToString(b)
}

/* Append */
func Append(ctx context.Context, channel, ctype string, content []byte,attrs ...string) (string,error) {
	if len(content) == 0 {
		return "-",ErrBadContent
	}

	if channel == "" {
		return "-",ErrBadChannel
	}

	/* channels can be created on the flow */
	chl,exists := internal.channels[ channel ]
	if !exists {
		chl = &Channel{Data: make([]*ChannelData,0)}
		internal.channels[ channel ] = chl
	}	

	chl.mux.Lock()
	defer chl.mux.Unlock()

	id := identifier()
	
	cd := &ChannelData{Meta: NewMeta(id, ctype)}
	cd.Content = make([]byte,len(content))
	copy(cd.Content,content)

	/* TODO: this needs to be seperated into a smaller function
	 * with better checking */
	for _,attr := range attrs {
		if strings.Contains(attr,"=") {
			parts := strings.Split(attr,"=")
			if len(parts) == 2 {
				if parts[0][0] == '+' {
					cd.Meta.Tags[parts[0][1:]] = parts[1]
				} else {
					cd.Meta.Attributes[parts[0]] = parts[1]
				}
			}
		}
	}

	chl.Data = append(chl.Data,cd)

	return id,nil
}

/* determines what to filter */ 
type Filter interface {

	Eval(meta *ChannelMetadata) bool
}



/* Next TODO: add a function that matches the next slot */
func Next(ctx context.Context, channel string, filters ...Filter) (string,[]byte,error) {
	if channel == "" {
		return "",nil,ErrBadChannel
	}

	/* gets the next content in the channel or a terminating stop error */
	chl,exists := internal.channels[ channel ]
	if !exists {
		return "",nil,ErrEmpty
	}
	
	chl.mux.RLock()
	defer chl.mux.RUnlock()

	if len(chl.Data) == 0 {
		return "",nil,ErrEmpty
	}

	if len(filters) == 0 {

		content := make([]byte,len(chl.Data[0].Content))
		copy(content,chl.Data[0].Content)	

		return chl.Data[0].Meta.ContentType,content,nil
	}

	for _, data := range chl.Data {	
		filtered := false	
		for _,f := range filters {
			if f.Eval(data.Meta.Copy()) {
				filtered = true
				break
			}
		}
		if !filtered {
			content := make([]byte,len(data.Content))
			copy(content,data.Content)

			return data.Meta.ContentType,content,nil
		}
	}

	return "",nil,ErrFiltered
}

/* NextInfo */
func NextInfo(ctx context.Context, channel string,filters ...Filter) (*ChannelMetadata,error) {

	if channel == "" {
		return nil,ErrBadChannel
	}

	chl,exists := internal.channels[ channel ]
	if !exists {
		return nil,ErrEmpty
	}
	
	chl.mux.RLock()
	defer chl.mux.RUnlock()

	if len(chl.Data) == 0 {
		return nil,ErrEmpty
	}

	if len(filters) == 0 {

		return chl.Data[0].Meta.Copy(),nil
	}

	for _,data := range chl.Data {
		filtered := false
		for _,f := range filters {
			if f.Eval(data.Meta.Copy()) {
				filtered = true
				break
			}
		}
		if !filtered {
			return data.Meta.Copy(),nil
		}
	}

	return nil,ErrFiltered
}
			

/* Size */
func Size(ctx context.Context, channel string) (int,error) {
	if channel == "" {
		return -1,ErrBadChannel
	}
		
	chl,exists := internal.channels[ channel ]
	if !exists {
		return -1,nil
	}

	chl.mux.RLock()
	defer chl.mux.RUnlock()


	return len(chl.Data),nil
}


/* Remove */
func Remove(ctx context.Context, channel string,filters ...Filter) (string,[]byte,error) {
	if channel == "" {
		return "",nil,ErrBadChannel
	}

	/* removes from the channel returning the content or a terminating stop error */
	chl,exists := internal.channels[ channel ]
	if !exists {
		return "",nil,ErrEmpty
	}

	chl.mux.Lock()
	defer chl.mux.Unlock()

	if len(chl.Data) == 0 {
		return "",nil,ErrEmpty
	}

	if len(filters) == 0 {

		meta := chl.Data[0].Meta
		content := make([]byte,len(chl.Data[0].Content))
		copy(content,chl.Data[0].Content)

		ndata := make([]*ChannelData,0)

		if len(chl.Data) > 1 {
			for i := 1; i < len(chl.Data); i++ {
				
				ndata = append(ndata,chl.Data[i])
			}
		}

		chl.Data = ndata

		return meta.ContentType,content,nil
	}

	var target = -1

	for i,data := range chl.Data {
		filtered := false
		for _,f := range filters {
			if f.Eval(data.Meta.Copy()) {
				filtered = true
				break
			}
		}
		if filtered {
			continue
		}
		
		target = i
		break
	}

	if target == -1 {
		return "",nil,ErrFiltered
	}

	meta := chl.Data[target].Meta
	content := make([]byte,len(chl.Data[target].Content))
	copy(content,chl.Data[target].Content)

	ndata := make([]*ChannelData,0)

	ndata = append(ndata,chl.Data[:target]...)
	if target < len(chl.Data) {
		ndata = append(ndata,chl.Data[target + 1:]...)
	}

	chl.Data = ndata

	return meta.ContentType,content,nil
}




/* UpdateInfo */
func UpdateInfo(ctx context.Context, info *ChannelMetadata) error {
	if info == nil || info.Id == "" {
		return ErrBadInfo
	}

	found := false	

	/* track down the data via id */
	for _,chl := range internal.channels {

		chl.mux.Lock()

		for _,data := range chl.Data {

				if data.Meta.Id != info.Id {
					continue	
				}

				/* we only copy the attributes */
				data.Meta.Tags = make(map[string]string,0)
				
				for key,value := range info.Tags {
					data.Meta.Tags[key] = value
				}

				found = true
				data.Meta.Version++
				break	
		}

		chl.mux.Unlock()
		
		if found {
			break
		}
	}

	return nil
}

/* Purge */
func Purge(ctx context.Context, channel string) error {
	if channel == "" {
		return ErrBadChannel
	}

	chl,exists := internal.channels[channel]
	if !exists {
		return ErrEmpty
	}

	chl.mux.Lock()
	defer chl.mux.Unlock()

	chl.Data = make([]*ChannelData,0)
	
	return nil
}





