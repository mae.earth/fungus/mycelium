/* mae.earth/fungus/mycelium/ll/store/seq_test.go
 * mae 012018 LYHE
 *
 * sequencal store, useful for queue implementation 
 */
package seq

import (
	"time"
	"context"
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

type filterHasFoo byte

func (filter *filterHasFoo) Eval(meta *ChannelMetadata) bool {
	if meta == nil {
		return false
	}

	if _,exists := meta.Attributes["foo"]; exists {
		return true
	}
	return false
}

func Test_Seq(t *testing.T) {
	Convey("Seq",t,func() {

		ctx := context.Background()

		/* put, get and delete specific; info based, putinfo, getinfo, deleteinfo */
		Convey("Data",func() {
			Convey("Append",func() {

				Convey("bad channel name",func() {
					id,err := Append(ctx,"","plain/text",[]byte("helo"))
					So(err,ShouldEqual,ErrBadChannel)
					So(id,ShouldEqual,"-")
				})

				Convey("good channel name",func() {					
					id,err := Append(ctx, "channel","plain/text",[]byte("helo"))
					So(err,ShouldBeNil)
					So(id,ShouldNotEqual,"-")
				})
			})

			Convey("Next",func() {

				Convey("bad channel name",func() {
					ctype,content,err := Next(ctx,"")
					So(err,ShouldEqual,ErrBadChannel)
					So(content,ShouldBeNil)
					So(ctype,ShouldBeEmpty)
				})

				Convey("from empty channel",func() {	
					ctype,content,err := Next(ctx, "foo")
					So(err,ShouldEqual,ErrEmpty)
					So(content,ShouldBeNil)
					So(ctype,ShouldBeEmpty)
				})

				Convey("from appended channel",func() {
					ctype,content,err := Next(ctx, "channel")
					So(err,ShouldBeNil)
					So(string(content),ShouldEqual,"helo")
					So(ctype,ShouldEqual,"plain/text")
				})
			})

			Convey("Size",func() {
	
				Convey("bad channel name", func() {
					size,err := Size(ctx,"")
					So(err,ShouldEqual,ErrBadChannel)
					So(size,ShouldEqual,-1)
				})

				Convey("from empty channel", func() {
					size,err := Size(ctx,"foo")
					So(err,ShouldBeNil)
					So(size,ShouldEqual,-1)
				})

				Convey("from appended channel", func() {
					size,err := Size(ctx,"channel")
					So(err,ShouldBeNil)
					So(size,ShouldEqual,1)
				})
			})

			Convey("Remove",func() {

				Convey("bad channel name", func() {
					ctype,content,err := Remove(ctx,"")
					So(err,ShouldEqual,ErrBadChannel)
					So(content,ShouldBeNil)
					So(ctype,ShouldBeEmpty)
				})

				Convey("from empty channel",func() {
					ctype,content,err := Remove(ctx, "foo")
					So(err,ShouldEqual,ErrEmpty)
					So(content,ShouldBeNil)
					So(ctype,ShouldBeEmpty)
				})
	
				Convey("from appended channel",func() {
					ctype,content,err := Remove(ctx, "channel")
					So(err,ShouldBeNil)
					So(string(content),ShouldEqual,"helo")
					So(ctype,ShouldEqual,"plain/text")
				})
			})

			Convey("Purge",func() {

				Convey("bad channel name",func() {
					So(Purge(ctx,""),ShouldEqual,ErrBadChannel)
				})
				Convey("from empty channel",func() {
					So(Purge(ctx,"foo"),ShouldEqual,ErrEmpty)
				})
				Convey("from appended channel",func() {
					So(Purge(ctx,"channel"),ShouldBeNil)
				})
			})
		})

		Convey("Info",func() {
			Convey("NextInfo",func() {
				/* infomation based api deals with 
				 * setting attributes in the metadata
  	     */	
				/* first make sure we have something in a channel */
				ctx := context.Background()					

				Convey("bad channel name",func() {
					meta,err := NextInfo(ctx,"")
					So(err,ShouldEqual,ErrBadChannel)
					So(meta,ShouldBeNil)
				})

				Convey("from empty channel",func() {
					meta,err := NextInfo(ctx,"foo")
					So(err,ShouldEqual,ErrEmpty)
					So(meta,ShouldBeNil)
				})

				Convey("from appended channel",func() {
								
					id,err := Append(ctx,"other","plain/text",[]byte("helo"))
					So(err,ShouldBeNil)

					meta,err := NextInfo(ctx,"other")
					So(err,ShouldBeNil)
					So(meta,ShouldNotBeNil)
					So(meta.Id,ShouldEqual,id)
					So(meta.Version,ShouldEqual,0)
					So(meta.ContentType,ShouldEqual,"plain/text")
					So(meta.Appended.Before(time.Now()),ShouldBeTrue)
					So(meta.Attributes,ShouldNotBeNil)
					So(len(meta.Attributes),ShouldEqual,0)

					meta.Attributes["foo"] = "bar"
					meta.Tags["foo"] = "bar"
	
					So(UpdateInfo(ctx,meta),ShouldBeNil)

					meta,err = NextInfo(ctx,"other")
					So(err,ShouldBeNil)
					So(meta,ShouldNotBeNil)
					So(meta.Attributes,ShouldNotBeNil)
					So(meta.Tags,ShouldNotBeNil)
					So(len(meta.Attributes),ShouldEqual,0)
					So(len(meta.Tags),ShouldEqual,1)
					So(meta.Tags["foo"],ShouldEqual,"bar")
					So(meta.Version,ShouldEqual,1)
				})
			})
		})

		Convey("More Complete",func() {
			Convey("append with Attributes",func() {
			
				id,err := Append(ctx,"example","plain/text",[]byte("Helo"),"foo=bar","+alice=in wonder land")
				So(err,ShouldBeNil)
				So(id,ShouldNotEqual,"-")
				
				meta,err := NextInfo(ctx,"example")
				So(err,ShouldBeNil)
				So(meta.Attributes,ShouldNotBeNil)
				So(len(meta.Attributes),ShouldEqual,1)
				So(meta.Attributes["foo"],ShouldEqual,"bar")
				
				So(meta.Tags,ShouldNotBeNil)
				So(len(meta.Tags),ShouldEqual,1)
				So(meta.Tags["alice"],ShouldEqual,"in wonder land")

				_,err = Append(ctx,"example","plain/text",[]byte("Bye"))
				So(err,ShouldBeNil)

				ctype,content,err := Next(ctx,"example",new(filterHasFoo))
				So(err,ShouldBeNil)
				So(ctype,ShouldEqual,"plain/text")
				So(string(content),ShouldEqual,"Bye")				


			})
		})
	})
}

