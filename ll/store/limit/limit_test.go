package limit

import (
	"bytes"
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_LimitStorage(t *testing.T) {
	
	Convey("Limit",t,func() {

		Convey("Namespace",func() {			
			ns := Namespace("foo")
			So(ns,ShouldNotBeEmpty)
			So(ns,ShouldEqual,"f7fbba6e0636f890e56fbbf3283e524c6fa3204ae298382d624741d0dc663832")		
		})

		Convey("Namespace Test",func() {
	
			l,err := Begin("./test.db")
			So(err,ShouldBeNil)
			So(l,ShouldNotBeNil)
		
			So(l.Write([]byte("hello there"),"soup","file"),ShouldBeNil)
								
			
			_,err = l.Read("soup","foo")
			So(err,ShouldEqual,ErrNotFound)
			
			out,err := l.Read("soup","file")
			So(err,ShouldBeNil)
			So(string(out),ShouldEqual,"hello there")
			
			files,err := l.Stat("soup")
			So(err,ShouldBeNil)
			So(len(files),ShouldEqual,1)
			So(files[0].Name,ShouldEqual,"file")
			So(files[0].Path,ShouldEqual,"/soup/file")
			So(files[0].IsBucket,ShouldBeFalse)
			So(files[0].Size,ShouldEqual,11)
			
			So(l.Delete("soup","file"),ShouldBeNil)

			files,err = l.Stat("soup")
			So(err,ShouldBeNil)
			So(len(files),ShouldEqual,0)
		
			So(l.End(),ShouldBeNil)

			So(Remove("./test.db"),ShouldBeNil)
		})

		Convey("Using buffers",func() {

			l,err := Begin("./test.db")
			So(err,ShouldBeNil)
			So(l,ShouldNotBeNil)

			buf := bytes.NewBuffer([]byte("hello there"))

			So(l.WriteFromBuffer(buf,"soup","file"),ShouldBeNil)

			buf.Reset()

			So(l.ReadToBuffer(buf,"soup","file"),ShouldBeNil)

			So(string(buf.Bytes()),ShouldEqual,"hello there")
					
			So(l.End(),ShouldBeNil)
			So(Remove("./test.db"),ShouldBeNil)

		})
	})
}
