package limit

/* this limit store works with bolt database, it uses the name of the database file as the hash(namespace) -- limit is the wrong term here dataset is better. */

/* TODO: need an audit path for every interaction etc, as this maybe used for security (rot layer) */

/* TODO: add a io.Writer, io.Reader interfaces */


import (
 	bolt "go.etcd.io/bbolt"

	"errors"
	"time"
	"path"
	"os"
	"fmt"
	"io"
	"bytes"

	"strconv"
	"crypto/sha512"
	"encoding/hex"

	"net/http"
)

const (
	NamespaceLimit int = 64
	DefaultMaxSize int = (1024 * 1024) * 5
)

func snip(in string) string {
	if len(in) <= NamespaceLimit {
		return in
	}

	return in[:NamespaceLimit]
}

var (
	ErrNotFound = errors.New("Not Found")
	ErrInvalidPath = errors.New("Invalid Path")
	ErrTooLarge = errors.New("Too Large")
	ErrBadContent = errors.New("Bad Content")
	ErrBadWriter = errors.New("Bad Writer")
)



/* Limit */
type Limit struct {
	path string
	namespace string
	db *bolt.DB
}

/* BackupOverHTTP : basic */
func (l *Limit) BackupOverHTTP(w http.ResponseWriter, req *http.Request) {
	err := l.db.View(func(tx *bolt.Tx) error {
		w.Header().Set("Content-Type", "application/octet-stream")
		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%q", l.namespace + "db"))
		w.Header().Set("Content-Length", strconv.Itoa(int(tx.Size())))
		_,err := tx.WriteTo(w)
		return err
	})
	if err != nil {
		http.Error(w,err.Error(),http.StatusInternalServerError)
	}
} 

/* WriteFromBuffer */
func (l *Limit) WriteFromBuffer(r io.Reader,parts ...string) error {

	if r == nil {
		return ErrBadContent
	}


	buf := bytes.NewBuffer(nil)
	if _,err := io.Copy(buf,r); err != nil {
		return err
	}

	if buf.Len() > DefaultMaxSize {
		return ErrTooLarge
	}

	return l.Write(buf.Bytes(),parts...) 
}




/* Write */
func (l *Limit) Write(content []byte,parts ...string) error {

	if len(content) > DefaultMaxSize {
		return ErrTooLarge
	}

	err := l.db.Update(func(tx *bolt.Tx) error {

		var b *bolt.Bucket
		var err error

		for _,part := range parts[:len(parts) - 1] {
			if b == nil {
				b,err = tx.CreateBucketIfNotExists([]byte(part))
				if err != nil {
					return err
				}
			} else {
				b,err = b.CreateBucketIfNotExists([]byte(part))
				if err != nil {	
					return err
				}
			}
		}

		if b == nil {
			return ErrInvalidPath
		}		

		return b.Put([]byte(parts[len(parts) - 1]),content)
	})

	return err
}

/* ReadToBuffer */
func (l *Limit) ReadToBuffer(w io.Writer,parts ...string) error {

	if w == nil {
		return ErrBadWriter
	}

	err := l.db.View(func(tx *bolt.Tx) error {

		var b *bolt.Bucket

		for i,part := range parts[:len(parts) - 1] {
			if b == nil && i == 0 {
				b = tx.Bucket([]byte(part))
			} else {
				b = b.Bucket([]byte(part))
			}

			if b == nil {
				return ErrNotFound
			}
		}

		if b == nil {
			return ErrNotFound
		}

		content := b.Get([]byte(parts[len(parts) - 1]))
		if content == nil {
			return ErrNotFound
		}
		
		_,err := w.Write(content)

		return err
	})

	return err	
}

/* Read */
func (l *Limit) Read(parts ...string) ([]byte,error) {

	var out []byte

	err := l.db.View(func(tx *bolt.Tx) error {

		var b *bolt.Bucket

		for i,part := range parts[:len(parts) - 1] {
			if b == nil && i == 0 {
				b = tx.Bucket([]byte(part))
			} else {
				b = b.Bucket([]byte(part))
			}

			if b == nil {
				return ErrNotFound
			}
		}

		if b == nil {
			return ErrNotFound
		}

		content := b.Get([]byte(parts[len(parts) - 1]))
		if content == nil {
			return ErrNotFound
		}
		out = make([]byte,len(content))
		copy(out,content)

		return nil 
	})

	return out,err
}

/* File */
type File struct {
	Name string
	Path string
	IsBucket bool 

	Size int
}

/* Stat */
func (l *Limit) Stat(parts ...string) ([]File,error) {

	files := make([]File,0)

	err := l.db.View(func(tx *bolt.Tx) error {
		
		var b *bolt.Bucket
	
		root := path.Join(parts...)

		for i,part := range parts {
			if b == nil && i == 0 {
				b = tx.Bucket([]byte(part))
			} else {
				b = b.Bucket([]byte(part))
			}

			if b == nil {
				return ErrNotFound
			}
		}

		if b == nil {
			return ErrNotFound
		}

		c := b.Cursor()

		for k,v := c.First(); k != nil; k,v = c.Next() {
			if v == nil { /* then bucket */

				s1 := b.Bucket(k).Stats()

				files = append(files,File{Name:string(k),Path:"/" + path.Join(root,string(k)),IsBucket:true,Size:s1.KeyN})
				/* TODO: look at the size of the bucket */
			} else {

				files = append(files,File{Name:string(k),Path:"/" + path.Join(root,string(k)),IsBucket:false,Size:len(v)})
			}
		}		

		return nil
	})

	return files,err
}


/* Delete */
func (l *Limit) Delete(parts ...string) error {

	err := l.db.Update(func(tx *bolt.Tx) error {
	
		var b *bolt.Bucket 

		for i,part := range parts[:len(parts) - 1] {
			if b == nil && i == 0 {
				b = tx.Bucket([]byte(part))
			} else {
				b = b.Bucket([]byte(part))
			}

			if b == nil {
				return ErrNotFound
			}
		}
	
		if b == nil {
			return ErrNotFound
		}

		name := []byte(parts[len(parts) - 1])

		/* check if file or bucket */
		if b.Bucket(name) == nil {
			
			return b.Delete(name)
		}		

		return b.DeleteBucket(name)
	})

	return err
}


/* End */
func (l *Limit) End() error {
	if l.db == nil {
		return nil
	}

	if err := l.db.Close(); err != nil {
		return err
	}

	return nil
}



/* Namespace */
func Namespace(name string) string {

	h := sha512.New()
	h.Write([]byte(name))

	return snip(hex.EncodeToString(h.Sum(nil)))
}


	

/* Begin */
func Begin(name string) (*Limit,error) {

	db,err := bolt.Open(name,0666,&bolt.Options{Timeout: 3 * time.Second})
	if err != nil {
		return nil,err
	}

	return &Limit{db:db,namespace: name},nil
}

/* BeginReadOnly */
func BeginReadOnly(name string) (*Limit,error) {
	
	db,err := bolt.Open(name,0666,&bolt.Options{ReadOnly: true, Timeout: 3 * time.Second})
	if err != nil {
		return nil,err
	}
	return &Limit{db:db,namespace:name},nil
}

/* Remove */
func Remove(name string) error {

	return os.Remove(name)
}	








