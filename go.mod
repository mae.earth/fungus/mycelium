module mae.earth/fungus/mycelium

go 1.14

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/awalterschulze/gographviz v0.0.0-20190522210029-fa59802746ab
	github.com/coreos/bbolt v1.3.3
	github.com/dustin/go-humanize v1.0.0
	github.com/gorilla/mux v1.7.4
	github.com/hashicorp/mdns v1.0.3
	github.com/olekukonko/tablewriter v0.0.4
	github.com/reusee/mmh3 v0.0.0-20140820141314-64b85163255b
	github.com/shirou/gopsutil v2.20.2+incompatible
	github.com/smartystreets/goconvey v1.6.4
	go.etcd.io/bbolt v1.3.3
	mae.earth/pkg/build v0.0.0-20200317092420-9676e33109ea
	mae.earth/pkg/sexpr v0.0.0-20181023150912-cb5ef6629036
	mae.earth/pkg/trustedtimestamps v0.0.0-20200317093537-b048252ef3b4
	mae.earth/pkg/vclock v0.0.0-20200317093654-c6e1aaaa210c
)
