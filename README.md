FUNGUS : MYCELIUM
==================

This is a collection of low-level local network resources that form a system for orchestration tools. 

TYPES of RESOURCES
------------------

+ QUEUE     :- a message orientated queue using bolt db
+ STORE     :- a data store system simular to Plan 9 Venti/Fossal. Also uses bolt db | zfs if available (as configured)
+ WORKNODE  :- does all the heavy lifting of running tasks on the network
+ CONFIG    :- provides all network configuration 
+ BOOTSTRAP :- useful for bootstrapping complex networks


