#!/bin/sh
user=$(whoami)
ver=$(cat VERSION)
date=$(date +%d-%b-%Y_%H:%M)

echo "building linux/amd64..."
go build -o bin/linux/mycelium -ldflags "-s -X gitlab.com/mae.earth/pkg/build.Version='${ver}' -X gitlab.com/mae.earth/pkg/build.User='${user}' -X gitlab.com/mae.earth/pkg/build.Date='${date}'"

echo "building freebsd/amd64..."
GOOS=freebsd GOARCH=amd64 go build -o bin/freebsd/mycelium -ldflags "-s -X gitlab.com/mae.earth/pkg/build.Version='${ver}' -X gitlab.com/mae.earth/pkg/build.User='${user}' -X gitlab.com/mae.earth/pkg/build.Date='${date}'"

echo "building windows/amd64..."
GOOS=windows GOARCH=amd64 go build -o bin/windows/mycelium -ldflags "-s -X gitlab.com/mae.earth/pkg/build.Version='${ver}' -X gitlab.com/mae.earth/pkg/build.User='${user}' -X gitlab.com/mae.earth/pkg/build.Date='${date}'"

#
#echo "building illumos/amd64..."
#GOOS=illumos GOARCH=amd64 go build -o bin/illumos/mycelium -ldflags "-s -X gitlab.com/mae.earth/pkg/build.Version='${ver}' -X gitlab.c$

echo "building solaris/amd64..."
GOOS=solaris GOARCH=amd64 go build -o bin/solaris/mycelium -ldflags "-s -X gitlab.com/mae.earth/pkg/build.Version='${ver}' -X gitlab.com/mae.earth/pkg/build.User='${user}' -X gitlab.com/mae.earth/pkg/build.Date='${date}'"


