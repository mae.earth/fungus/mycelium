/* fungus/mycelium/host/host_test.go
 * mae 012018
 */
package host

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_BasicInformation(t *testing.T) {
	Convey("Basic Information",t,func() {

		info,err := GetInformation(LightProfile)
		So(err,ShouldBeNil)
		So(info,ShouldNotBeNil)
		So(info,ShouldNotBeEmpty)

		So(info.Get("platform"),ShouldNotBeEmpty)
		So(info.Get("ram"),ShouldNotBeEmpty)	
	})

	Convey("Make Identity",t,func() {
		Convey("no arguments",func() {
			So(MakeIdentity(),ShouldEqual,"c21f8cdfedd25a5efd0b83905269a173e17bff8c") /* FIXME: this test relies on the hostname being marvin */
		})

		Convey("component + weight",func() {
			Convey("worknode + 10",func() {
				So(MakeIdentity("worknode",10),ShouldEqual,"36111ef8db76339e6ee7c6cbefb5873797bdbde6")
			})
			Convey("worknode + 11",func() {
				So(MakeIdentity("worknode",11),ShouldEqual,"33b2453d2bf13d06042d6b743b01a0e17781a816")
			})
		})
		
	
	})
}


