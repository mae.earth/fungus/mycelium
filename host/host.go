/* fungus/mycelium/host/host.go
 * mae 012018
 */
package host

import (
	"fmt"
	"strings"
	"strconv"
	"os"
	"crypto/sha1"
	"encoding/hex"

/*	"github.com/dustin/go-humanize" */
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/mem"
)

/* Profile */
const (
	LightProfile string = "platform;ram"
)


/* Information */
type Information map[string]string

/* Get */
func (info Information) Get(key string) string {
	if v,ok := info[key]; ok {
		return v
	}
	return ""
}

/* GetAsInt */
func (info Information) GetAsInt(key string,def int) int {
	if v,ok := info[key]; ok {

		d,err := strconv.ParseInt(v,10,64)
		if err != nil {
			return def
		}
		
		return int(d)
	}
	return def
}

/* GetInformation */
func GetInformation(profile string) (Information,error) {

	info := make(map[string]string,0)

	hinfo,err := host.Info()
	if err != nil {
		return nil,err
	}

	vinfo,err := mem.VirtualMemory()
	if err != nil {
		return nil,err
	}

	for _,key := range strings.Split(profile,";") {

		key = strings.TrimSpace(key)

		switch strings.ToLower(key) {
			case "platform":
				platform := fmt.Sprintf("%s/%s", hinfo.OS, hinfo.Platform)
				if hinfo.OS == hinfo.Platform {
					platform = hinfo.OS
				}
			
				info["platform"] = platform
			break
			case "ram":
				info["ram"] = fmt.Sprintf("%d", vinfo.Total)
			break
		}

		/* TODO: add more info */
	}

	return info,nil
}

/* MakeIdentity */
func MakeIdentity(attrs ...interface{}) string {

	hostname, _ := os.Hostname()

	h := sha1.New()
	h.Write([]byte(hostname))
	
	for _,attr := range attrs {

		if v,ok := attr.(string); ok {
			h.Write([]byte("+"))
			h.Write([]byte(v))
		}
		if v,ok := attr.(float64); ok {
			h.Write([]byte("+"))
			h.Write([]byte(fmt.Sprintf("%f",v)))
		}
		if v,ok := attr.(int); ok {
			h.Write([]byte("+"))
			h.Write([]byte(fmt.Sprintf("%d",v)))
		}
	}	
		
	return hex.EncodeToString(h.Sum(nil))
}





